package com.amatrus.codeforcesbrowser.data.database

import com.amatrus.codeforcesbrowser.contests.list.Contest
import com.amatrus.codeforcesbrowser.contests.list.Contest_
import com.amatrus.codeforcesbrowser.problemset.problems.list.*
import com.amatrus.codeforcesbrowser.reminders.list.Reminder
import com.amatrus.codeforcesbrowser.reminders.list.Reminder_
import io.objectbox.kotlin.boxFor

/**
 * Created by engnaruto on 12/10/17.
 */


class DatabaseRepository {

    private var contestsBox = App.boxStore.boxFor<Contest>()
    private var problemsetBox = App.boxStore.boxFor<Problem>()
    private var remindersBox = App.boxStore.boxFor<Reminder>()
    private var problemStatementsBox = App.boxStore.boxFor<ProblemStatement>()
    private var tagsBox = App.boxStore.boxFor<Tag>()


    fun getAllContests(isGym: Boolean): List<Contest> {
        return contestsBox.query().equal(Contest_.isGym, isGym)
                .orderDesc(Contest_.startTimeSeconds).build().find()
    }

    fun addContests(contestsList: List<Contest>, isGym: Boolean) {
        if (isGym) {
            contestsList.forEach { it -> it.isGym = isGym }
        }
        contestsBox.put(contestsList)
    }

    fun removeContests(contestCids: List<Long>) {
        contestsBox.removeByKeys(contestCids)
    }

    fun getNumberOfContests(isGym: Boolean): Int {
        return contestsBox.query().equal(Contest_.isGym, isGym).build().count().toInt()
    }

    fun getAllProblemset(): List<Problem> {
        return problemsetBox.query().orderDesc(Problem_.id).build().find()
    }

    fun addProblems(problemsList: List<Problem>) {
        problemsetBox.put(problemsList.reversed())
    }

    fun getProblemsOfContest(contestId: Long): List<Problem> {
        return problemsetBox.query().equal(Problem_.contestId, contestId)
                .order(Problem_.index).build().find()
    }

    fun getProblemsLikeTitle(query: String): List<Problem> {
        return problemsetBox.query().contains(Problem_.name, query)
                .orderDesc(Problem_.contestId).build().find()
    }

    fun getNumberOfProblems(): Int {
        return problemsetBox.all.size
    }

    fun addProblemStatement(problemStatement: ProblemStatement) {
        problemStatementsBox.put(problemStatement)
    }

    fun getProblemStatement(problemId: Long): String {
        return problemStatementsBox.query().equal(ProblemStatement_.pid, problemId)
                .build().findFirst()?.problemStatement ?: ""
    }

    fun getAllReminders(): List<Reminder> {
        return remindersBox.query().orderDesc(Reminder_.contestStartingTime).build().find()
    }

    fun addReminder(reminder: Reminder) {
        remindersBox.put(reminder)
    }

    fun removeReminder(rid: Long) {
        remindersBox.remove(rid)
    }


    fun getTag(name: String): Tag? {
        return tagsBox.query().equal(Tag_.name, name).build().findFirst()
    }

    companion object {

        private var instance: DatabaseRepository? = null

        fun create(): DatabaseRepository {
            if (instance == null) {
                instance = DatabaseRepository()
            }
            return instance as DatabaseRepository
        }
    }
}