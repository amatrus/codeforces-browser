package com.amatrus.codeforcesbrowser.data.api

import com.amatrus.codeforcesbrowser.contests.list.ContestsResult
import com.amatrus.codeforcesbrowser.problemset.problems.list.ProblemsetResult
import com.amatrus.codeforcesbrowser.problemset.submissions.list.SubmissionsResult
import com.amatrus.codeforcesbrowser.user.info.UserResult
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import io.reactivex.Observable

/**
 * Created by naruto on 11/5/17.
 */

interface CodeforcesApiService {


    @GET("contest.list")
    fun getContestsList(@Query("gym") isGym: Boolean): Observable<ContestsResult>

    @GET("user.info")
    fun getUsersList(@Query("handles") users: String): Observable<UserResult>

    @GET("problemset.problems")
    fun getProblemsList(@Query("tags") tags: String): Observable<ProblemsetResult>

    @GET("problemset.recentStatus")
    fun getSubmissionsList(@Query("count") count: Int): Observable<SubmissionsResult>

    companion object Factory {
        private const val BASE_URL: String = "http://codeforces.com/api/"

        fun create(): CodeforcesApiService {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build()
            return retrofit.create(CodeforcesApiService::class.java)
        }
    }
}