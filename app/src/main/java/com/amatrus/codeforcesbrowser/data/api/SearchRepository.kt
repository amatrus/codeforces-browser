package com.amatrus.codeforcesbrowser.data.api

import com.amatrus.codeforcesbrowser.contests.list.ContestsResult
import com.amatrus.codeforcesbrowser.problemset.problems.list.ProblemsetResult
import com.amatrus.codeforcesbrowser.problemset.submissions.list.SubmissionsResult
import com.amatrus.codeforcesbrowser.user.info.UserResult
import io.reactivex.Observable

/**
 * Created by engnaruto on 11/5/17.
 */
class SearchRepository(private val apiService: CodeforcesApiService) {

    fun getContestsList(isGym: Boolean): Observable<ContestsResult> =
            apiService.getContestsList(isGym)

//    fun getUsersList(userHandles: List<String>): Observable<UserResult> {
//        val query = userHandles.joinToString { ";" }
//
//        return apiService.getUsersList(query)
//    }

    fun getUser(handle: String): Observable<UserResult> = apiService.getUsersList(handle)


    fun getAllProblemset(): Observable<ProblemsetResult> = apiService.getProblemsList("")

    fun getSubmissionsList(count: Int): Observable<SubmissionsResult> = apiService.getSubmissionsList(count)

//    fun getProblemsList(tags: List<String>): Observable<ProblemsetResult> {
//        val query = tags.joinToString(";")
//        return apiService.getProblemsList(query)
//    }

}