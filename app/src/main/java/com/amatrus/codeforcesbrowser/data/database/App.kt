package com.amatrus.codeforcesbrowser.data.database

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.support.v7.preference.PreferenceManager
import com.amatrus.codeforcesbrowser.MyObjectBox
import io.objectbox.BoxStore
import android.net.ConnectivityManager
import com.firebase.jobdispatcher.FirebaseJobDispatcher
import com.firebase.jobdispatcher.GooglePlayDriver


/**
 * Created by engnaruto on 12/5/17.
 */


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        app = this
        boxStore = MyObjectBox.builder().androidContext(App.app).build()
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        dispatcher = FirebaseJobDispatcher(GooglePlayDriver(app))
    }

    companion object {
        lateinit var app: App
        lateinit var boxStore: BoxStore
        lateinit var sharedPreferences: SharedPreferences
        lateinit var dispatcher: FirebaseJobDispatcher

        fun getStringFromResources(resId: Int): String {
            return app.getString(resId)
        }

        fun isOnline(): Boolean {
            val cm = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            return netInfo != null && netInfo.isConnected
        }
    }
}