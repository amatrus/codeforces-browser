package com.amatrus.codeforcesbrowser.data.api

/**
 * Created by naruto on 11/5/17.
 */

object SearchRepositoryProvider{
    fun provideSearchRepository(): SearchRepository{
        return SearchRepository(CodeforcesApiService.create())
    }
}