package com.amatrus.codeforcesbrowser.extensions

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by engnaruto on 11/23/17.
 */

fun Int.convertTimeSecondsToDate(regex: String): String {
    val simpleDateFormat = SimpleDateFormat(regex, Locale.ENGLISH)
    simpleDateFormat.timeZone = TimeZone.getDefault()
    return simpleDateFormat.format(this * 1000L)
}

fun Long.convertTimeSecondsToDate(regex: String): String {
    val simpleDateFormat = SimpleDateFormat(regex, Locale.ENGLISH)
    simpleDateFormat.timeZone = TimeZone.getDefault()
    return simpleDateFormat.format(this)
}

fun Int.convertDurationTimeToHours(): String {
    val convertTimeToMinutes: Int = this / 60
    val hours = convertTimeToMinutes / 60
    val remainingMinutes = convertTimeToMinutes - hours * 60
    return String.format("%02d:%02d", hours, remainingMinutes)
}