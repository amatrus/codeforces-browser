package com.amatrus.codeforcesbrowser.settings.about

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.databinding.AboutActivityBinding

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: AboutActivityBinding = DataBindingUtil.setContentView(this, R.layout.about_activity)
        binding.viewModel = AboutViewModel()
        initializeToolBar()
    }

    private fun initializeToolBar() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }
}