package com.amatrus.codeforcesbrowser.settings.settings

import android.content.Intent
import android.view.View
import com.amatrus.codeforcesbrowser.settings.about.AboutActivity

/**
 * Created by engnaruto on 12/14/17.
 */

class SettingsViewModel {

    fun onClickAboutButton(view: View) {
        val intent = Intent(view.context, AboutActivity::class.java)
        view.context.startActivity(intent)
    }
}