package com.amatrus.codeforcesbrowser.settings.settings


import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat
import com.amatrus.codeforcesbrowser.R


/**
 * Created by engnaruto on 12/10/17.
 */

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.settings_preferences)
    }
}// Required empty public constructor
