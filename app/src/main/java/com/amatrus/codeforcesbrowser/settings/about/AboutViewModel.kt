package com.amatrus.codeforcesbrowser.settings.about

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View

/**
 * Created by engnaruto on 12/14/17.
 */

class AboutViewModel {

    fun onClickSendFeedbackButton(view: View) {
        sendEmail(view.context)
    }

    private fun sendEmail(context: Context) {
        val intent = Intent(Intent.ACTION_SENDTO,
                Uri.fromParts("mailto", "tarekcsed@gmail.com", null))
        intent.putExtra(Intent.EXTRA_SUBJECT, "Codeforces Browser User Feedback")
        intent.putExtra(Intent.EXTRA_TEXT, "")
        context.startActivity(Intent.createChooser(intent, "Send email via"))
    }
}