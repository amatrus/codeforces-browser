package com.amatrus.codeforcesbrowser.problemset.tags

import android.databinding.ObservableInt
import android.view.View
import com.amatrus.codeforcesbrowser.base.BaseListViewModel
import com.amatrus.codeforcesbrowser.problemset.problems.list.Tag

class TagsDialogViewModel(tags: List<Tag>) : BaseListViewModel() {

    var tagsList: List<Tag> = tags
    var textViewNoTagsVisibility: ObservableInt = ObservableInt(View.GONE)


    init {
        if (tagsList.isEmpty()) {
            textViewNoTagsVisibility.set(View.VISIBLE)
        }
    }

    fun getTagsList() {
        tagsList
        setChanged()
        notifyObservers()
    }
}