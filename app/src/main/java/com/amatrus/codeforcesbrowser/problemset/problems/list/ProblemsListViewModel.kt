package com.amatrus.codeforcesbrowser.problemset.problems.list


import android.util.Log
import android.widget.Toast
import com.amatrus.codeforcesbrowser.base.BaseListViewModel
import com.amatrus.codeforcesbrowser.data.database.App
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by engnaruto on 11/25/17.
 */


class ProblemsListViewModel : BaseListViewModel() {

    lateinit var problemsList: List<Problem>

    fun getProblemsList() {
        if (App.isOnline()) {
            getDataFromApi()
        } else {
            Toast.makeText(App.app, NO_INTERNET_CONNECTION_ERROR_MESSAGE, Toast.LENGTH_SHORT).show()
            setNewProblemsList(getDataFromDatabase())
            showResults()
        }
    }

    private fun getDataFromDatabase(): List<Problem> {
        return databaseRepository.getAllProblemset()
    }

    private fun getDataFromApi() {
        repository.getAllProblemset()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    processResults(result)
                }, { error ->
                    error.printStackTrace()
                    setNewProblemsList(getDataFromDatabase())
                    showResults()
                    Toast.makeText(App.app, LOADING_DATA_FROM_SERVER_ERROR_MESSAGE, Toast.LENGTH_SHORT)
                            .show()
                })
    }

    private fun processResults(result: ProblemsetResult) {
        Log.e("Result Status", result.status)
        if (result.status == "OK") {
            addSolvedCountToProblemsList(result.result)
            Log.e("Result", "There are ${result.result.problems.size} problems")
            val apiProblems = result.result.problems
            Log.e("Problem Result", apiProblems[0].toString())

            val newProblems = mutableListOf<Problem>()

            for (problem in apiProblems) {
                newProblems.add(Problem.getInstance(problem))
            }

            val numberOfProblems = databaseRepository.getNumberOfProblems()

            if (numberOfProblems != 0) {
                val databaseProblemsList = getDataFromDatabase()
                var i = 0

                while (newProblems[i].contestId != databaseProblemsList[0].contestId
                        || newProblems[i].index != databaseProblemsList[0].index) {
                    i++
                }

                if (i != 0) {
                    val slicedNewProblems = newProblems.slice(IntRange(0, i))
                    databaseRepository.addProblems(slicedNewProblems)
                }
            } else {
                databaseRepository.addProblems(newProblems)
            }
        } else {
            showErrorMessage(LOADING_DATA_FROM_SERVER_ERROR_MESSAGE)
        }

        val problemsList = getDataFromDatabase()
        setNewProblemsList(problemsList)
        showResults()
    }

    private fun addSolvedCountToProblemsList(result: Result) {
        for (i in 0 until result.problems.size) {
            result.problems[i].solvedCount = result.problemStatistics[i].solvedCount
        }
    }

    private fun setNewProblemsList(resultsList: List<Problem>) {
        problemsList = resultsList
        setChanged()
        notifyObservers()
    }
}