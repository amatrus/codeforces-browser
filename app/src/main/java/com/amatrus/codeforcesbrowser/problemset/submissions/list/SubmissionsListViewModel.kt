package com.amatrus.codeforcesbrowser.problemset.submissions.list


import com.amatrus.codeforcesbrowser.base.BaseListViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by engnaruto on 1/24/18.
 */


class SubmissionsListViewModel : BaseListViewModel() {

    lateinit var submissionsList: List<Submission>
    val NUMBER_OF_SUBMISSIONS = 100


    fun getSubmissionsList() {
        getDataFromApi()
    }

    private fun getDataFromApi() {
        repository.getSubmissionsList(NUMBER_OF_SUBMISSIONS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    processResults(result)
                }, { error ->
                    error.printStackTrace()
                    showErrorMessage(LOADING_DATA_FROM_SERVER_ERROR_MESSAGE)
                })
    }

    private fun processResults(result: SubmissionsResult) {
        if (result.status == "OK") {
            submissionsList = result.result
            setNewSubmissionsList(submissionsList)
            showResults()
        } else {
            showErrorMessage(LOADING_DATA_FROM_SERVER_ERROR_MESSAGE)
        }
    }

    private fun setNewSubmissionsList(resultsList: List<Submission>) {
        submissionsList = resultsList
        setChanged()
        notifyObservers()
    }
}