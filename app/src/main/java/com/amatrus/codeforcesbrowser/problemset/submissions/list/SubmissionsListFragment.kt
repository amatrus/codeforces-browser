package com.amatrus.codeforcesbrowser.problemset.submissions.list


import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.base.BaseListFragment
import com.amatrus.codeforcesbrowser.databinding.SubmissionsListFragmentBinding
import java.util.*


/**
 * Created by engnaruto on 1/24/18.
 */
class SubmissionsListFragment : BaseListFragment() {
    private lateinit var binding: SubmissionsListFragmentBinding
    private lateinit var submissionsListViewModel: SubmissionsListViewModel
    private lateinit var submissionsListAdapter: SubmissionsListAdapter

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.submissions_list_fragment, container, false)
        submissionsListAdapter = SubmissionsListAdapter()
        initializeBinding()
        setUpRecyclerView(binding.recyclerview, 1, 10, submissionsListAdapter)
        setupObserver(submissionsListViewModel)
        submissionsListViewModel.getSubmissionsList()
        return binding.root
    }

    private fun initializeBinding() {
        submissionsListViewModel = SubmissionsListViewModel()
        binding.viewModel = submissionsListViewModel
    }

    override fun update(p0: Observable?, p1: Any?) {
        when (p0) {
            is SubmissionsListViewModel -> {
                val adapter = binding.recyclerview.adapter as SubmissionsListAdapter
                adapter.setSubmissionsList(p0.submissionsList)
            }
        }
    }

    override fun beginSearch(query: String) {
        submissionsListAdapter.filter.filter(query)
    }

    companion object {
        fun newInstance(): SubmissionsListFragment = SubmissionsListFragment()
    }

}// Required empty public constructor
