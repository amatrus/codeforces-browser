package com.amatrus.codeforcesbrowser.problemset.problems.list

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.support.customtabs.CustomTabsIntent
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.data.database.App
import com.amatrus.codeforcesbrowser.problemset.problems.webview.WebViewActivity
import com.amatrus.codeforcesbrowser.problemset.tags.TagsDialogFragment


/**
 * Created by engnaruto on 11/25/17.
 */

class ProblemItemViewModel(private val problem: Problem) {

    private val PROBLEM_URL =
            "http://codeforces.com/problemset/problem/${problem.contestId}/${problem.index}"
    private val SHARE_URL_DESCRIPTION =
            "Check ${problemId()} ${problem.name} problem @codeforces $PROBLEM_URL"
    private val PROBLEM_NAME_CHARACTER_COUNT = 100

    fun problemName(): String {
        if (problem.name.length > PROBLEM_NAME_CHARACTER_COUNT) {
            return "${problem.name.substring(0, PROBLEM_NAME_CHARACTER_COUNT)}..."
        }
        return problem.name
    }

    fun problemCount(): String {
        return "x${problem.solvedCount}"
    }

    fun problemId(): String {
        return "${problem.contestId}${problem.index}"
    }

    fun onClickProblemItem(view: View) {
        val sharedPreferences = App.sharedPreferences
        val isUsingWebView = sharedPreferences
                .getBoolean(App.getStringFromResources(R.string.pref_use_webview), true)
        if (isUsingWebView) {
            parseProblemStatementInWebView(view.context)
        } else {
            initializeChromeTab(view)
        }
    }

    private fun parseProblemStatementInWebView(context: Context) {
        val intent = Intent(context, WebViewActivity::class.java)
        intent.putExtra("PROBLEM_URL", PROBLEM_URL)
        intent.putExtra("PROBLEM_ID", problem.id)
        context.startActivity(intent)
    }

    fun onClickShareButton(view: View) {
        shareContest(view.context)
    }

    fun onClickProblemTagsButton(view: View) {
        val activity = view.context as AppCompatActivity
        showTagsDialog(activity.supportFragmentManager, problem.tags)
    }


    private fun showTagsDialog(fragmentManager: FragmentManager, tags: List<Tag>) {
        val alertDialog = TagsDialogFragment.newInstance(tags)
        alertDialog.show(fragmentManager, "fragment_alert")
    }

    private fun shareContest(context: Context) {
        val intent = prepareSharingIntent()
        context.startActivity(Intent.createChooser(intent, "Share via"))
    }

    private fun prepareSharingIntent(): Intent {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, SHARE_URL_DESCRIPTION)
        return intent
    }

    private fun initializeChromeTab(view: View) {
        // Use a CustomTabsIntent.Builder to configure CustomTabsIntent.
        val builder = CustomTabsIntent.Builder()

        // set toolbar color
        builder.setToolbarColor(ContextCompat.getColor(view.context, R.color.colorPrimaryDark))

        val requestCode = 100

        val intent = prepareSharingIntent()

        val pendingIntent = PendingIntent.getActivity(
                view.context,
                requestCode,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        )

        // currently, Chrome Tabs do not support vector drawables
        // so you should be using PNG files as your icons
        val bitmap = BitmapFactory.decodeResource(view.resources, R.drawable.ic_share)

        // add share action to menu list
        //     builder.addDefaultShareMenuItem()
        // set share button
        builder.setActionButton(bitmap, "Share Link", pendingIntent, true)

        // set toolbar color and/or setting custom actions before invoking build()
        // Once ready, call CustomTabsIntent.Builder.build() to create a CustomTabsIntent
        val customTabsIntent = builder.build()

        // and launch the desired Url with CustomTabsIntent.launchUrl()
        customTabsIntent.launchUrl(view.context, Uri.parse(PROBLEM_URL))
    }
}