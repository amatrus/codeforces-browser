package com.amatrus.codeforcesbrowser.problemset.parent

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.amatrus.codeforcesbrowser.problemset.problems.list.ProblemsListFragment
import com.amatrus.codeforcesbrowser.problemset.submissions.list.SubmissionsListFragment

/**
 * Created by engnaruto on 11/25/17.
 */
class ProblemsetParentPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
    private val NUMBER_OF_FRAGMENTS = 2


    override fun getItem(position: Int): Fragment = when (position) {
        0 -> ProblemsListFragment.newInstance()
        1 -> SubmissionsListFragment.newInstance()
        else -> ProblemsListFragment.newInstance()
    }

    override fun getCount(): Int = NUMBER_OF_FRAGMENTS

    override fun getPageTitle(position: Int): CharSequence? = when (position) {
        0 -> "Problemset"
        1 -> "Submissions"
        else -> "N/A"
    }
}