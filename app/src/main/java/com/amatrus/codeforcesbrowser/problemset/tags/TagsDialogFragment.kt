package com.amatrus.codeforcesbrowser.problemset.tags

import android.app.AlertDialog
import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.base.SpacingItemDecoration
import com.amatrus.codeforcesbrowser.databinding.TagsDialogFragmentBinding
import com.amatrus.codeforcesbrowser.problemset.problems.list.Tag
import java.util.*


class TagsDialogFragment : DialogFragment(), Observer {

    private lateinit var binding: TagsDialogFragmentBinding
    private lateinit var tagsDialogViewModel: TagsDialogViewModel
    private lateinit var tagsListAdapter: TagsListAdapter

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.tags_dialog_fragment, null, false)

        tagsListAdapter = TagsListAdapter()

        val tags: Array<Tag> = arguments?.getParcelableArray(TAGS_LIST_TAG) as Array<Tag>
        Log.e("DIALOGG", tags.map { it -> it.name }.toString())

        initializeBinding(tags.toList())
        setUpRecyclerView(binding.recyclerview, 1, 10, tagsListAdapter)
        setupObserver(tagsDialogViewModel)
        tagsDialogViewModel.getTagsList()


        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Tags")
                .setView(binding.root)
                .setNegativeButton("Close") { dialog, _ ->
                    dialog.dismiss()
                }
        return builder.create()
    }

    private fun initializeBinding(tagsList: List<Tag>) {
        tagsDialogViewModel = TagsDialogViewModel(tagsList)
        binding.viewModel = tagsDialogViewModel
    }

    private fun setUpRecyclerView(recyclerView: RecyclerView, spanCount: Int, gapSize: Int, listAdapter: RecyclerView.Adapter<*>) {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addItemDecoration(SpacingItemDecoration(spanCount, dpToPx(gapSize), true))
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = listAdapter
    }

    /**
     * Converting dp to pixel
     */
    private fun dpToPx(dp: Int): Int {
        val r = resources
        return Math.round(
                TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        dp.toFloat(),
                        r.displayMetrics
                )
        )
    }

    fun setupObserver(observable: Observable) {
        observable.addObserver(this)
    }

    override fun update(p0: Observable?, p1: Any?) {
        when (p0) {
            is TagsDialogViewModel -> {
                val adapter = binding.recyclerview.adapter as TagsListAdapter
                adapter.setTagsList(p0.tagsList)
            }
        }
    }


    companion object {
        const val TAGS_LIST_TAG = "tagsList"

        fun newInstance(tagsList: List<Tag>): TagsDialogFragment {
            val tags = tagsList.map { it -> it.name }.toString()

            Log.e("DIALOGX", tags)
            val frag = TagsDialogFragment()
            val args = Bundle()
            args.putParcelableArray(TAGS_LIST_TAG, tagsList.toTypedArray())
            frag.arguments = args
            return frag
        }
    }
}