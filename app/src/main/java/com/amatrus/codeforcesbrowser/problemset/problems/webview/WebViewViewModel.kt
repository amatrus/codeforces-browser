package com.amatrus.codeforcesbrowser.problemset.problems.webview

import android.os.AsyncTask
import android.util.Log
import com.amatrus.codeforcesbrowser.base.BaseViewModel
import com.amatrus.codeforcesbrowser.data.database.App
import com.amatrus.codeforcesbrowser.data.database.DatabaseRepository
import com.amatrus.codeforcesbrowser.problemset.problems.list.ProblemStatement
import org.jsoup.Jsoup
import java.io.IOException

/**
 * Created by engnaruto on 12/18/17.
 */

class WebViewViewModel(private val problemUrl: String, private val problemId: Long) : BaseViewModel() {
    private val databaseRepository = DatabaseRepository.create()

    var problemStatement: String = ""
        private set

    fun getProblemStatement() {

        problemStatement = databaseRepository.getProblemStatement(problemId)
        if (problemStatement.isEmpty()) {
            if (App.isOnline()) {
                getDataFromApi()
                if (problemStatement.isEmpty()) {
                    showErrorMessage(NO_INTERNET_CONNECTION_ERROR_MESSAGE)
                } else {
                    databaseRepository.addProblemStatement(ProblemStatement(0, problemId, problemStatement))
                    setChanged()
                    notifyObservers()
                    showResults()
                }
            } else {
                showErrorMessage(NO_INTERNET_CONNECTION_ERROR_MESSAGE)
            }
        } else {
            setChanged()
            notifyObservers()
            showResults()
        }
    }

    private fun getDataFromApi() {
        val sync = Sync()
        problemStatement = sync.execute(problemUrl).get()
    }


    private inner class Sync : AsyncTask<String, String, String>() {

        override fun doInBackground(vararg params: String): String {
            var problemStatementToString = ""
            try {
                val problemPage = Jsoup.connect(params[0]).get()
                //TODO: Handle if connection timed out
                val problemStatement = problemPage.getElementsByClass("problemindexholder")
                val emptyPage = "<html><head></head><body><body></html>"
                val problemStatementPage = Jsoup.parse(emptyPage)
                problemStatementPage.head().appendElement("link").attr("rel", "stylesheet").attr("type", "text/css").attr("href", "file:///android_asset/main.css")
                problemStatementPage.body().append(problemStatement.html())

                val problemImages = problemStatementPage.body().getElementsByTag("img")
                problemImages.forEach { it.attr("src", "http://codeforces.com${it.attr("src")}") }

                problemStatementToString = problemStatementPage.toString()
            } catch (e: IOException) {
                Log.e("PROBLEM", e.printStackTrace().toString())
            }
            return problemStatementToString
        }
    }
}