package com.amatrus.codeforcesbrowser.problemset.problems.list

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.data.database.DatabaseRepository
import com.amatrus.codeforcesbrowser.databinding.ProblemItemBinding

/**
 * Created by engnaruto on 11/25/17.
 */


class ProblemsListAdapter : RecyclerView.Adapter<ProblemsListAdapter.ProblemItemViewHolder>(),
    Filterable {

    private var problemsList: List<Problem> = listOf()
    private val problemsFilter = ProblemsFilter()

    fun setProblemsList(list: List<Problem>) {
        problemsList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ProblemItemViewHolder, position: Int) {
        holder.bind(problemsList[position])
    }

    override fun getItemCount(): Int = problemsList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProblemItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ProblemItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.problem_item, parent, false)

        return ProblemItemViewHolder(binding)
    }

    override fun getFilter(): Filter {
        return problemsFilter
    }

    class ProblemItemViewHolder(private val binding: ProblemItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Problem) {
            binding.viewModel = ProblemItemViewModel(data)
        }
    }

    inner class ProblemsFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val databaseRepository = DatabaseRepository.create()
            val results = Filter.FilterResults()
            val filterList: List<Problem>

            filterList = if (constraint != null && constraint.isNotEmpty()) {
                databaseRepository.getProblemsLikeTitle(constraint.toString())
            } else {
                databaseRepository.getAllProblemset()
            }

            results.count = filterList.size
            results.values = filterList
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            problemsList = results!!.values as List<Problem>
            notifyDataSetChanged()
        }
    }
}