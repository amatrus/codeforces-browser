package com.amatrus.codeforcesbrowser.problemset.submissions.list

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.databinding.SubmissionItemBinding

/**
 * Created by engnaruto on 1/24/18.
 */

class SubmissionsListAdapter :
    RecyclerView.Adapter<SubmissionsListAdapter.SubmissionItemViewHolder>(), Filterable {

    private var submissionsList: List<Submission> = listOf()
    private var originalSubmissionsList: List<Submission> = listOf()
    private val submissionsFilter = SubmissionsFilter()

    fun setSubmissionsList(list: List<Submission>) {
        submissionsList = list
        originalSubmissionsList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: SubmissionItemViewHolder, position: Int) {
        holder.bind(submissionsList[position])
    }

    override fun getItemCount(): Int = submissionsList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubmissionItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: SubmissionItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.submission_item, parent, false)

        return SubmissionItemViewHolder(binding)
    }

    override fun getFilter(): Filter {
        return submissionsFilter
    }

    class SubmissionItemViewHolder(private val binding: SubmissionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Submission) {
            binding.viewModel = SubmissionItemViewModel(data)
        }
    }

    inner class SubmissionsFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val results = Filter.FilterResults()
            val filterList: List<Submission>

            filterList = if (constraint != null && constraint.isNotEmpty()) {
                originalSubmissionsList.filter { it -> it.problem.name.contains(constraint, true) }
            } else {
                originalSubmissionsList
            }

            results.count = filterList.size
            results.values = filterList
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            submissionsList = results!!.values as List<Submission>
            notifyDataSetChanged()
        }
    }
}