package com.amatrus.codeforcesbrowser.problemset.submissions.list

import com.amatrus.codeforcesbrowser.problemset.problems.list.ProblemModel
import com.google.gson.annotations.SerializedName

/**
 * Created by engnaruto on 1/24/18.
 */

data class SubmissionsResult(
        @SerializedName("status") val status: String = "", //OK
        @SerializedName("result") val result: List<Submission> = listOf()
)

data class Submission(
        @SerializedName("id") val id: Int = 0, //34442882
        @SerializedName("contestId") val contestId: Int = 0, //914
        @SerializedName("creationTimeSeconds") val creationTimeSeconds: Int = 0, //1516616856
        @SerializedName("relativeTimeSeconds") val relativeTimeSeconds: Int = 0, //2147483647
        @SerializedName("problem") val problem: ProblemModel = ProblemModel(),
        @SerializedName("author") val author: Author = Author(),
        @SerializedName("programmingLanguage") val programmingLanguage: String = "", //GNU C++11
        @SerializedName("verdict") val verdict: String = "", //TESTING
        @SerializedName("testset") val testset: String = "", //TESTS
        @SerializedName("passedTestCount") val passedTestCount: Int = 0, //0
        @SerializedName("timeConsumedMillis") val timeConsumedMillis: Int = 0, //0
        @SerializedName("memoryConsumedBytes") val memoryConsumedBytes: Int = 0 //0
)

data class Author(
        @SerializedName("contestId") val contestId: Int = 0, //914
        @SerializedName("members") val members: List<Member> = listOf(),
        @SerializedName("participantType") val participantType: String = "", //PRACTICE
        @SerializedName("ghost") val ghost: Boolean = false, //false
        @SerializedName("startTimeSeconds") val startTimeSeconds: Int = 0 //1516462500
)

data class Member(
        @SerializedName("handle") val handle: String = "" //Osama_Alkhodairy
)