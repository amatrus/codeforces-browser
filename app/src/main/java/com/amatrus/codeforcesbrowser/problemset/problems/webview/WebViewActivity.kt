package com.amatrus.codeforcesbrowser.problemset.problems.webview

import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.webkit.WebSettings
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.databinding.WebviewActivityBinding
import java.util.*

class WebViewActivity : AppCompatActivity(), Observer {

    private lateinit var binding: WebviewActivityBinding
    private lateinit var webViewViewModel: WebViewViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.webview_activity)

        initializeToolBar()

        initializeWebView()

        val problemUrl = intent.getStringExtra("PROBLEM_URL")
        val problemId = intent.getLongExtra("PROBLEM_ID",-1)

        initializeBinding(problemUrl,problemId)

        setupObserver(webViewViewModel)

        webViewViewModel.getProblemStatement()
    }

    private fun initializeBinding(problemUrl: String, problemId: Long) {
        webViewViewModel = WebViewViewModel(problemUrl,problemId)
        binding.viewModel = webViewViewModel
    }

    private fun initializeWebView() {
        initializeWebViewZoomControls()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.webview.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }
    }

    private fun initializeToolBar() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    private fun initializeWebViewZoomControls() {
        binding.webview.settings.builtInZoomControls = true
        binding.webview.settings.displayZoomControls = false
    }

    private fun setupObserver(observable: Observable) {
        observable.addObserver(this)
    }

    override fun update(viewModel: Observable?, p1: Any?) {
        when (viewModel) {
            is WebViewViewModel -> {
                binding.webview.loadDataWithBaseURL(null,
                        viewModel.problemStatement,
                        "text/html",
                        "UTF-8",
                        null
                )
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }
}