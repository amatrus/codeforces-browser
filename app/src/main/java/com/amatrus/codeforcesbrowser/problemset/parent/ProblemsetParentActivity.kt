package com.amatrus.codeforcesbrowser.problemset.parent

import android.app.SearchManager
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.base.BaseListFragment
import com.amatrus.codeforcesbrowser.databinding.ProblemsetParentFragmentBinding

/**
 * Created by engnaruto on 11/25/17.
 */

class ProblemsetParentActivity : AppCompatActivity() {

    val title = "Problemset"
    private lateinit var tabLayout: TabLayout
    internal var searchView: SearchView? = null
    private lateinit var viewPager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ProblemsetParentFragmentBinding =
            DataBindingUtil.setContentView(this, R.layout.problemset_parent_fragment)

        tabLayout = binding.slidingTabs
        viewPager = binding.userViewPager
        val toolbar = binding.toolbar

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        initializeTabLayout(tabLayout, viewPager)
    }

    private fun initializeTabLayout(tabLayout: TabLayout, viewPager: ViewPager) {
        val problemsetParentPagerAdapter = ProblemsetParentPagerAdapter(supportFragmentManager)

        viewPager.adapter = problemsetParentPagerAdapter
        tabLayout.setupWithViewPager(viewPager)

        // Attach the page change listener inside the activity
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            // This method will be invoked when a new page becomes selected.
            override fun onPageSelected(position: Int) {
                if (searchView != null && !searchView!!.isIconified) {
                    //searchView.onActionViewExpanded();
                    searchView!!.isIconified = true
                }
            }

            // This method will be invoked when the current page is scrolled
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                // Code goes here
                if (searchView != null && !searchView!!.isIconified) {
                    searchView!!.isIconified = true
                }
            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            override fun onPageScrollStateChanged(state: Int) {
                // Code goes here
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.problem_search_menu, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val mSearchMenuItem = menu.findItem(R.id.action_search)
        searchView = mSearchMenuItem.actionView as SearchView

        if (searchView != null) {
            searchView!!.setSearchableInfo(searchManager.getSearchableInfo(this.componentName))
            searchView!!.setIconifiedByDefault(true)
        }

        val queryTextListener = object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(query: String): Boolean {

                val viewPagerFragment =
                    viewPager.adapter!!.instantiateItem(
                        viewPager,
                        viewPager.currentItem
                    ) as BaseListFragment

                if (viewPagerFragment.isAdded) {
                    viewPagerFragment.beginSearch(query)
                }
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // Here u can get the value "query" which is entered in the
                return false
            }
        }
        searchView!!.setOnQueryTextListener(queryTextListener)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}