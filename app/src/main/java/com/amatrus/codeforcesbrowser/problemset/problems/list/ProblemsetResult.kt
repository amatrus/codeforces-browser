package com.amatrus.codeforcesbrowser.problemset.problems.list

import android.os.Parcel
import android.os.Parcelable
import com.amatrus.codeforcesbrowser.data.database.DatabaseRepository
import com.google.gson.annotations.SerializedName
import io.objectbox.annotation.Backlink
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.relation.ToMany


/**
 * Created by engnaruto on 11/23/17.
 */


data class ProblemsetResult(
        @SerializedName("status") val status: String = "", //OK
        @SerializedName("result") val result: Result = Result()
)

data class Result(
        @SerializedName("problems") val problems: List<ProblemModel> = listOf(),
        @SerializedName("problemStatistics") val problemStatistics: List<ProblemStatistic> = listOf()
)


/*
* This is the class for getting the problems from the API
*/

//@Entity
data class ProblemModel(
        @SerializedName("contestId") val contestId: Int = 0, //892
        @SerializedName("index") val index: String = "", //B
        @SerializedName("name") val name: String = "", //Wrath
        @SerializedName("type") val type: String = "", //PROGRAMMING
        @SerializedName("points") val points: Double = 0.0, //1000.0
        @SerializedName("tags") val tags: List<String> = mutableListOf(),
        var solvedCount: Int = 0 //3656
)


/*
* This is the class for getting the problems from the Database
*/

@Entity
class Problem(@Id
              var id: Long = 0,
              var contestId: Int = 0, //892
              var index: String = "", //B
              var name: String = "", //Wrath
              var type: String = "", //PROGRAMMING
              var points: Double = 0.0, //1000.0
              var solvedCount: Int = 0 //3656
) {


    lateinit var tags: ToMany<Tag>

    companion object {
        fun getInstance(problem: ProblemModel): Problem {
            val newProblem = Problem(0, problem.contestId, problem.index, problem.name, problem.type, problem.points, problem.solvedCount)

            val databaseRepository = DatabaseRepository.create()
            for (name in problem.tags) {
                val tag = databaseRepository.getTag(name) ?: Tag(0, name)
                newProblem.tags.add(tag)
            }
            return newProblem
        }
    }
}

@Entity
class Tag(
        @Id var id: Long = 0,
        val name: String
) : Parcelable {
    @Backlink(to = "tags")
    lateinit var problems: ToMany<Problem>

    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Tag> {
        override fun createFromParcel(parcel: Parcel): Tag {
            return Tag(parcel)
        }

        override fun newArray(size: Int): Array<Tag?> {
            return arrayOfNulls(size)
        }
    }
}

@Entity
data class ProblemStatement(
        @Id var id: Long = 0,
        val pid: Long = -1,
        val problemStatement: String = ""
)

data class ProblemStatistic(
        @SerializedName("contestId") val contestId: Int = 0, //892
        @SerializedName("index") val index: String = "", //B
        @SerializedName("solvedCount") val solvedCount: Int = 0 //3656
)