package com.amatrus.codeforcesbrowser.problemset.submissions.list

import android.databinding.BindingAdapter
import android.support.v4.content.ContextCompat
import android.widget.TextView
import com.amatrus.codeforcesbrowser.R

/**
 * Created by engnaruto on 1/24/18.
 */

class SubmissionItemViewModel(private val submission: Submission) {

    fun submissionId(): String {
        return submission.id.toString()
    }

    fun submissionAuthorHandle(): String {
        return submission.author.members[0].handle
    }

    fun submissionLang(): String {
        return submission.programmingLanguage
    }

    fun submissionTime(): String {
        return "${submission.timeConsumedMillis} ms"
    }

    fun submissionMemory(): String {
        return "${submission.memoryConsumedBytes / 1024} KB"
    }

    fun submissionResult(): String {
        return if (submission.verdict == "TIME_LIMIT_EXCEEDED") {
            "TIME_LIMIT"
        } else if (submission.verdict == "MEMORY_LIMIT_EXCEEDED") {
            "MEMORY_LIMIT"
        } else {
            submission.verdict
        }
    }

    fun problemName(): String {
        return "${submission.contestId}${submission.problem.index} - ${submission.problem.name}"
    }

    object TextViewBindingAdapter {

        private val resultColorMap = mapOf(
                Pair("OK", R.color.userColorGreen),
                Pair("TESTING", R.color.userColorOrange),
                Pair("WRONG_ANSWER", R.color.userColorRed),
                Pair("RUNTIME_ERROR", R.color.userColorBlue),
                Pair("TIME_LIMIT", R.color.userColorCyan),
                Pair("MEMORY_LIMIT", R.color.userColorViolet)
        )

        private fun getResultColor(result: String): Int =
                resultColorMap[result] ?: R.color.userColorBlack

        @BindingAdapter("resultColor")
        @JvmStatic
        fun setFont(textView: TextView, result: String) {
            textView.setTextColor(
                    ContextCompat.getColor(textView.context, getResultColor(result))
            )
        }
    }
}