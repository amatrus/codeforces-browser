package com.amatrus.codeforcesbrowser.problemset.tags

import com.amatrus.codeforcesbrowser.problemset.problems.list.Tag

class TagItemViewModel(private val tag: Tag) {

    fun tagName(): String {
        return tag.name
    }
}