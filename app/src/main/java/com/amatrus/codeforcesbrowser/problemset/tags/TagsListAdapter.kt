package com.amatrus.codeforcesbrowser.problemset.tags

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.databinding.TagItemBinding
import com.amatrus.codeforcesbrowser.problemset.problems.list.Tag

class TagsListAdapter : RecyclerView.Adapter<TagsListAdapter.TagItemViewHolder>() {

    private var tagsList: List<Tag> = listOf()

    fun setTagsList(list: List<Tag>) {
        tagsList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: TagItemViewHolder, position: Int) {
        holder.bind(tagsList[position])
    }

    override fun getItemCount(): Int = tagsList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TagItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: TagItemBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.tag_item, parent, false)

        return TagItemViewHolder(binding)
    }

    class TagItemViewHolder(private val binding: TagItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Tag) {
            binding.viewModel = TagItemViewModel(data)
        }
    }
}