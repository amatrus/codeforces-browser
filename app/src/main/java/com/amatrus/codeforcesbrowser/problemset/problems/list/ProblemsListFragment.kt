package com.amatrus.codeforcesbrowser.problemset.problems.list


import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.base.BaseListFragment
import com.amatrus.codeforcesbrowser.databinding.ProblemsListFragmentBinding
import java.util.*


/**
 * Created by engnaruto on 11/25/17.
 */

class ProblemsListFragment : BaseListFragment() {
    private lateinit var binding: ProblemsListFragmentBinding
    private lateinit var problemsListViewModel: ProblemsListViewModel
    private lateinit var problemsListAdapter: ProblemsListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
                DataBindingUtil.inflate(inflater, R.layout.problems_list_fragment, container, false)
        problemsListAdapter = ProblemsListAdapter()
        initializeBinding()
        setUpRecyclerView(binding.recyclerviewProblems, 1, 10, problemsListAdapter)
        setupObserver(problemsListViewModel)
        problemsListViewModel.getProblemsList()
        return binding.root
    }

    private fun initializeBinding() {
        problemsListViewModel = ProblemsListViewModel()
        binding.viewModel = problemsListViewModel
    }

    override fun update(p0: Observable?, p1: Any?) {
        when (p0) {
            is ProblemsListViewModel -> {
                val adapter = binding.recyclerviewProblems.adapter as ProblemsListAdapter
                adapter.setProblemsList(p0.problemsList)
            }
        }
    }

    override fun beginSearch(query: String) {
        problemsListAdapter.filter.filter(query)
    }

    companion object {
        fun newInstance(): ProblemsListFragment = ProblemsListFragment()
    }

}// Required empty public constructor
