package com.amatrus.codeforcesbrowser

import android.content.Intent
import android.content.SharedPreferences
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.amatrus.codeforcesbrowser.contests.parent.ContestsParentFragment
import com.amatrus.codeforcesbrowser.databinding.MainActivityBinding
import com.amatrus.codeforcesbrowser.databinding.NavigationDrawerHeaderBinding
import com.amatrus.codeforcesbrowser.navigation.EditUserHandleDialog
import com.amatrus.codeforcesbrowser.navigation.UserHeaderViewModel
import com.amatrus.codeforcesbrowser.problemset.parent.ProblemsetParentActivity
import com.amatrus.codeforcesbrowser.reminders.list.RemindersListFragment
import com.amatrus.codeforcesbrowser.settings.settings.SettingsActivity
import com.amatrus.codeforcesbrowser.user.parent.UserParentFragment
import kotlinx.android.synthetic.main.main_activity.*
import kotlinx.android.synthetic.main.main_fragment_container_layout.*
import java.util.*

/**
 * Created by engnaruto on 10/31/17.
 */

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
        Observer {
    lateinit var binding: MainActivityBinding
    private lateinit var headerBinding: NavigationDrawerHeaderBinding
    private lateinit var userHeaderViewModel: UserHeaderViewModel

    private lateinit var sharedPreferences: SharedPreferences
    lateinit var handle: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.main_activity)

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        initializeNavigationDrawerHeaderBinding(binding)
        initializeNavigationDrawerListener()
        setContestsView()
        setupObserver(userHeaderViewModel)
    }

    private fun initializeNavigationDrawerHeaderBinding(binding: MainActivityBinding) {
        handle = sharedPreferences.getString("handle", "").trim()

        userHeaderViewModel = UserHeaderViewModel()
        userHeaderViewModel.getUser(handle)
        val headerView = binding.navigationDrawerView.getHeaderView(0)
        headerBinding = DataBindingUtil.bind(headerView)!!
        headerBinding.viewModel = userHeaderViewModel
    }

    private fun setupObserver(observable: Observable) {
        observable.addObserver(this)
    }

    override fun update(p0: Observable?, p1: Any?) {
        if (p0 is UserHeaderViewModel) {
            headerBinding.viewModel = userHeaderViewModel
        }
    }

    private fun setContestsView() {
        binding.navigationDrawerView.setCheckedItem(R.id.nav_contests)
        startAndReplaceFragment(ContestsParentFragment.newInstance())
    }

    private fun initializeNavigationDrawerListener() {
        binding.navigationDrawerView.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_my_account -> {
                // Handle the my account action
                handle = sharedPreferences.getString("handle", "").trim()
                Log.e("MainNewHandleBeforeeeee", handle)
                if (handle != "") {
                    startFragment(UserParentFragment.newInstance())
                } else {
//                    Toast.makeText(this, "MainNewHandleBefore - $handle", Toast.LENGTH_SHORT).show()

                    val editUserHandleDialog = EditUserHandleDialog.newInstance(userHeaderViewModel)
                    editUserHandleDialog.show(supportFragmentManager, "EditUserHandleDialog")

                    drawer_layout.closeDrawer(GravityCompat.START)

                    handle = sharedPreferences.getString("handle", "").trim()

                    if (handle != "") {
                        startFragment(UserParentFragment.newInstance())
                    }
                }
            }
            R.id.nav_contests -> {
                // Handle the contests action
                startFragment(ContestsParentFragment.newInstance())
            }
            R.id.nav_alarms -> {
                // Handle the reminders action
                startFragment(RemindersListFragment.newInstance())
            }
            R.id.nav_problemset -> {
                // Handle the problemset action
                val intent = Intent(this, ProblemsetParentActivity::class.java)
                startActivity(intent)
            }
//            R.id.nav_users -> {
//                // Handle the users action
//                Toast.makeText(this, "Go to Users Fragment", Toast.LENGTH_SHORT).show()
//            }
//            R.id.nav_recent -> {
//                // Handle the recent actions action
//                Toast.makeText(this, "Go to Recent Actions Fragment", Toast.LENGTH_SHORT).show()
//            }
            R.id.nav_settings -> {
                // Handle the settings action
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun startFragment(fragment: Fragment) {
        val firstFragment = supportFragmentManager.fragments.first()
        val lastFragment = supportFragmentManager.fragments.last()
//        Log.e("firstFragment", firstFragment.javaClass.simpleName.split(".").last())
//        Log.e("lastFragment", lastFragment.javaClass.simpleName.split(".").last())
//        Log.e("fragment", fragment.javaClass.simpleName.split(".").last())

        if (fragment.javaClass != lastFragment.javaClass) {
            if (supportFragmentManager.backStackEntryCount > 0) {
                if (fragment.javaClass == firstFragment.javaClass) {
                    supportFragmentManager.popBackStack()
                } else {
                    supportFragmentManager.popBackStack()
                    addFragmentToBackStack(fragment)
                }
            } else {
                addFragmentToBackStack(fragment)
            }
        }
    }

    private fun addFragmentToBackStack(fragment: Fragment) {
        supportFragmentManager.beginTransaction().add(fragment_container_layout.id, fragment)
                .addToBackStack(null).commit()
    }

    private fun startAndReplaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(fragment_container_layout.id, fragment)
                .commit()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
            if (supportFragmentManager.backStackEntryCount >= 0) {
                val currentFragment =
                        supportFragmentManager.findFragmentById(R.id.fragment_container_layout) as Fragment
                when (currentFragment) {
                    is UserParentFragment -> binding.navigationDrawerView.setCheckedItem(R.id.nav_my_account)
                    is ContestsParentFragment -> binding.navigationDrawerView.setCheckedItem(R.id.nav_contests)
                }
            }
        }
    }
}