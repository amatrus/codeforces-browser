package com.amatrus.codeforcesbrowser.navigation

import android.app.AlertDialog
import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.databinding.EditUserHandleDialogFragmentBinding

/**
 * Created by engnaruto on 11/28/17.
 */

class EditUserHandleDialog : DialogFragment() {


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val userHeaderViewModel: UserHeaderViewModel = arguments?.getParcelable(USER_HEADER_VIEWMODEL_LIST_TAG)!!

        val builder = AlertDialog.Builder(activity)

        val editUserHandleDialogViewModel = EditUserHandleDialogViewModel()

        val binding: EditUserHandleDialogFragmentBinding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.edit_user_handle_dialog_fragment, null, false)


        binding.viewModel = editUserHandleDialogViewModel

        builder.setView(binding.root)
                .setTitle("Register User")
                .setMessage("Add your handle")
                .setIcon(R.drawable.ic_header_edit)
                .setPositiveButton(android.R.string.yes) { _, _ ->

                    val newHandle = editUserHandleDialogViewModel.userHandle.get()!!
                    Log.e("NEW HANDLE", newHandle)
                    if (newHandle != "") {
                        setNewHandle(newHandle)
                        userHeaderViewModel.getUser(newHandle)
                    }
                }
                .setNegativeButton(android.R.string.no) { _, _ ->
                    // Nothing to do here
                }

        return builder.create()
    }

    private fun setNewHandle(newHandle: String) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString("handle", newHandle)
        editor.apply()
    }


    companion object {
        const val USER_HEADER_VIEWMODEL_LIST_TAG = "userHeaderViewModel"

        fun newInstance(userHeaderViewModel: UserHeaderViewModel): EditUserHandleDialog {
            val frag = EditUserHandleDialog()
            val args = Bundle()
            args.putParcelable(USER_HEADER_VIEWMODEL_LIST_TAG, userHeaderViewModel)
            frag.arguments = args
            return frag
        }
    }
}