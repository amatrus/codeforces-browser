package com.amatrus.codeforcesbrowser.navigation

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.BindingAdapter
import android.databinding.ObservableField
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText


class EditUserHandleDialogViewModel : BaseObservable() {

    @Bindable
    val userHandle: ObservableField<String> = ObservableField("")

    @Bindable
    fun getUserHandleTextWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // Do nothing.
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                userHandle.set(s.toString())
            }

            override fun afterTextChanged(s: Editable) {
                // Do nothing.
            }
        }
    }

    object EditTextBindingAdapters {

        @BindingAdapter("textChangedListener")
        @JvmStatic
        fun bindTextWatcher(editText: EditText, textWatcher: TextWatcher) {
            editText.addTextChangedListener(textWatcher)
        }
    }
}