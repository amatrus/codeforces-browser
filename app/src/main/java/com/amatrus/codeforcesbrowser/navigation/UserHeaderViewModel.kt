package com.amatrus.codeforcesbrowser.navigation

import android.os.Parcel
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.View
import com.amatrus.codeforcesbrowser.base.BaseUserViewModel

/**
 * Created by engnaruto on 11/27/17.
 */

class UserHeaderViewModel() : BaseUserViewModel(), Parcelable {

    fun onClickEditButton(view: View) {
        Log.e("Header", "Clicked Edit Button")
        val contextThemeWrapper = view.context as ContextThemeWrapper
        val activity = contextThemeWrapper.baseContext as AppCompatActivity
        val editUserHandleDialog = EditUserHandleDialog.newInstance(this)
        editUserHandleDialog.show(activity.supportFragmentManager, "EditUserHandleDialog")
    }

    constructor(parcel: Parcel) : this()

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserHeaderViewModel> {
        override fun createFromParcel(parcel: Parcel): UserHeaderViewModel {
            return UserHeaderViewModel(parcel)
        }

        override fun newArray(size: Int): Array<UserHeaderViewModel?> {
            return arrayOfNulls(size)
        }
    }
}