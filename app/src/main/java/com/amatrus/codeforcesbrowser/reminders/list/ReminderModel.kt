package com.amatrus.codeforcesbrowser.reminders.list

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id


/**
 * Created by engnaruto on 12/20/17.
 */

@Entity
data class Reminder(
        @Id var id: Long = 0,
        val contestId: Int = 0, //892
        val contestName: String = "", //B
        val contestStartingTime: Int = -1,
        val tag: String = "" //Wrath
)