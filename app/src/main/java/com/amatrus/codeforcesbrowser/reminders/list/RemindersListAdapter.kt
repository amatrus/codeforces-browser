package com.amatrus.codeforcesbrowser.reminders.list

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.databinding.ReminderItemBinding

/**
 * Created by engnaruto on 12/20/17.
 */


class RemindersListAdapter : RecyclerView.Adapter<RemindersListAdapter.ReminderItemViewHolder>() {

    private var remindersList: List<Reminder> = listOf()


    fun setRemindersList(list: List<Reminder>) {
        remindersList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ReminderItemViewHolder, position: Int) {
        holder.bind(remindersList[position])
    }

    override fun getItemCount(): Int = remindersList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReminderItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ReminderItemBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.reminder_item, parent, false)

        return ReminderItemViewHolder(binding)
    }


    class ReminderItemViewHolder(private val binding: ReminderItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Reminder) {
            binding.viewModel = ReminderItemViewModel(data)
        }
    }
}