package com.amatrus.codeforcesbrowser.reminders.notifications

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import com.amatrus.codeforcesbrowser.MainActivity
import com.amatrus.codeforcesbrowser.R


/**
 * Created by engnaruto on 12/19/17.
 */


class NotificationBuilder(val context: Context) {


    private val mNotificationManager: NotificationManager by lazy {
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }


    init {

        // Create the channel object with the unique ID
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val remindersChannel = NotificationChannel(
                    REMINDERS_CHANNEL,
                    context.getString(R.string.reminders_notification_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT
            )

            // Configure the channel's initial settings
            remindersChannel.lightColor = Color.GREEN
            remindersChannel.setShowBadge(true)

            // Submit the notification channel object to the notification manager
            mNotificationManager.createNotificationChannel(remindersChannel)
        }

    }

    fun showContestReminderNotification(reminderId: Int, contestName: String, contestId: Int) {

        val notificationBuilder = createNotificationBuilder(contestName)

        addActionsToNotification(contestId, reminderId, notificationBuilder)

        addSoundAndVibrationToNotification(notificationBuilder)

        addPendingIntentToNotification(notificationBuilder)

        val notification = notificationBuilder.build()

        notification.flags = notification.flags or Notification.FLAG_AUTO_CANCEL // Dismiss notification after clicking it

        showNotification(context, notification, reminderId)
    }

    private fun addPendingIntentToNotification(notificationBuilder: NotificationCompat.Builder) {
        val resultPendingIntent = PendingIntent.getActivity(context, 0, Intent(context, MainActivity::class.java), 0)
        notificationBuilder.setContentIntent(resultPendingIntent)
    }

    private fun addActionsToNotification(contestId: Int, reminderId: Int, notificationBuilder: NotificationCompat.Builder) {

        val registerAction = createRegisterAction(contestId)
        val dismissAction = createDismissAction(reminderId)

        notificationBuilder.addAction(registerAction)
        notificationBuilder.addAction(dismissAction)
    }

    private fun createDismissAction(reminderId: Int): NotificationCompat.Action? {
        val dismissIntent = createDismissPendingIntent(context, reminderId)
        return NotificationCompat.Action.Builder(android.R.drawable.sym_def_app_icon, DISMISS_LABEL, dismissIntent).build()
    }

    private fun createRegisterAction(contestId: Int): NotificationCompat.Action? {
        val contestRegistrationLink = "http://codeforces.com/contestRegistration/$contestId"
        val registerIntent = createRegistrationPendingIntent(contestRegistrationLink, context)
        return NotificationCompat.Action.Builder(android.R.drawable.sym_def_app_icon, REGISTER_LABEL, registerIntent).build()
    }

    private fun addSoundAndVibrationToNotification(notificationBuilder: NotificationCompat.Builder) {
        val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        notificationBuilder.setSound(uri)
        notificationBuilder.setVibrate(longArrayOf(1000, 1000, 1000))
    }

    private fun createRegistrationPendingIntent(contestRegistrationLink: String, context: Context): PendingIntent? {
        val registrationIntent = Intent(Intent.ACTION_VIEW, Uri.parse(contestRegistrationLink))

        return PendingIntent.getActivity(context, 0, registrationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun createDismissPendingIntent(context: Context, reminderId: Int): PendingIntent? {
        val dismissIntent = Intent(context, ButtonReceiver::class.java)
        dismissIntent.putExtra("notificationId", reminderId)

        return PendingIntent.getBroadcast(context, 0, dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun createNotificationBuilder(contestName: String): NotificationCompat.Builder {

        val notificationTitle = "$contestName Contest Reminder"
        val notificationBody = "Don't forget to register in $contestName contest"
        val largeIcon = BitmapFactory.decodeResource(context.resources,
                R.drawable.ic_codeforces)

        return NotificationCompat.Builder(context, REMINDERS_CHANNEL)
                .setSmallIcon(R.drawable.ic_codeforces)
                .setContentTitle(notificationTitle)
                .setContentText(notificationBody)
                .setLargeIcon(largeIcon)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setStyle(NotificationCompat.BigTextStyle().bigText(notificationBody))
                .setAutoCancel(true)
    }

    private fun showNotification(context: Context, notification: Notification, id: Int) {
        val mNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.notify(id, notification)
    }

    companion object {
        const val REGISTER_LABEL = "Register"
        const val DISMISS_LABEL = "Dismiss"
        const val REMINDERS_CHANNEL = "reminder"
    }
}