package com.amatrus.codeforcesbrowser.reminders.notifications

import com.amatrus.codeforcesbrowser.data.database.DatabaseRepository
import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService

/**
 * Created by engnaruto on 12/20/17.
 */

class NotificationJobService : JobService() {

    private val CONTEST_NAME = "contest-name"
    private val CONTEST_ID = "contest-id"
    private val REMINDER_ID = "reminder-id"

    override fun onStartJob(job: JobParameters): Boolean {
        // Do some work here
        val notificationBuilder = NotificationBuilder(this)

        val bundle = job.extras
        var contestName = "NONE"
        if (bundle?.getString(CONTEST_NAME) != null) {
            contestName = bundle.getString(CONTEST_NAME) as String
        }

        var contestId = -1
        if (bundle?.getInt(CONTEST_ID) != null) {
            contestId = bundle.getInt(CONTEST_ID)
        }

        var reminderId = -1
        if (bundle?.getInt(REMINDER_ID) != null) {
            reminderId = bundle.getInt(REMINDER_ID)
        }

        notificationBuilder.showContestReminderNotification(reminderId, contestName, contestId)

        removeReminderFromDatabase(reminderId)

        return false // Answers the question: "Is there still work going on?"
    }

    private fun removeReminderFromDatabase(reminderId: Int) {
        val databaseRepository = DatabaseRepository()
        databaseRepository.removeReminder(reminderId.toLong())
    }

    override fun onStopJob(job: JobParameters): Boolean {
        return true // Answers the question: "Should this job be retried?"
    }
}