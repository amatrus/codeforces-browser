package com.amatrus.codeforcesbrowser.reminders.notifications

import android.os.Bundle
import android.util.Log
import com.amatrus.codeforcesbrowser.data.database.App
import com.amatrus.codeforcesbrowser.reminders.list.Reminder
import com.firebase.jobdispatcher.Job
import com.firebase.jobdispatcher.Lifetime
import com.firebase.jobdispatcher.RetryStrategy
import com.firebase.jobdispatcher.Trigger


class NotificationReminder(val reminder: Reminder) {

    private lateinit var job: Job

    private val CONTEST_NAME = "contest-name"
    private val CONTEST_ID = "contest-id"
    private val REMINDER_ID = "reminder-id"


    fun create(): Boolean {

        val currentTimeSeconds = (System.currentTimeMillis() / 1000).toInt()

        if (reminder.contestStartingTime <= currentTimeSeconds) {
            return false
        }

        val startingNotificationInterval = reminder.contestStartingTime - currentTimeSeconds - 3600 // notification will be an hour before the contest
        val endingNotificationInterval = startingNotificationInterval + 600 // 10 minutes interval

        val myExtrasBundle = Bundle()
        myExtrasBundle.putString(CONTEST_NAME, reminder.contestName)
        myExtrasBundle.putInt(REMINDER_ID, reminder.id.toInt())
        myExtrasBundle.putInt(CONTEST_ID, reminder.contestId)

        Log.e("REMINDER_ID CREATE", myExtrasBundle.getInt(REMINDER_ID).toString())

        job = App.dispatcher.newJobBuilder()
                // the JobService that will be called
                .setService(NotificationJobService::class.java)
                // uniquely identifies the job
                .setTag(reminder.tag)
                // one-off job
                .setRecurring(false)
                // don't persist past a device reboot
                .setLifetime(Lifetime.FOREVER)
                // start between 0 and 60 seconds from now
                .setTrigger(Trigger.executionWindow(startingNotificationInterval, endingNotificationInterval))
                // don't overwrite an existing job with the same tag
                .setReplaceCurrent(false)
                // retry with exponential backoff
                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                .setExtras(myExtrasBundle)
                .build()

        App.dispatcher.mustSchedule(job)

        return true
    }
}
