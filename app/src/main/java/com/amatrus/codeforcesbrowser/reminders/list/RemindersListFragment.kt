package com.amatrus.codeforcesbrowser.reminders.list


import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amatrus.codeforcesbrowser.base.BaseListFragment

import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.databinding.RemindersListFragmentBinding
import java.util.*


/**
 * Created by engnaruto on 12/20/17.
 */
class RemindersListFragment : BaseListFragment() {
    private lateinit var binding: RemindersListFragmentBinding
    private lateinit var remindersListViewModel: RemindersListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.reminders_list_fragment, container, false)
        initializeBinding()
        initializeToolBar(binding.toolbar, "Reminders")
        setUpRecyclerView(binding.recyclerview, 1, 10, RemindersListAdapter())
        setupObserver(remindersListViewModel)
        remindersListViewModel.getRemindersList()
        return binding.root
    }

    private fun initializeToolBar(toolbar: Toolbar, title: String) {
        val activity = activity as AppCompatActivity

        activity.setSupportActionBar(toolbar)
        activity.title = title

        val drawer = activity.findViewById<View>(R.id.drawer_layout) as DrawerLayout

        val toggle = ActionBarDrawerToggle(
                activity, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)

        drawer.addDrawerListener(toggle)

        toggle.syncState()

        toolbar.setNavigationOnClickListener({
            //open navigation drawer when click navigation back button
            drawer.openDrawer(GravityCompat.START)
        })
    }

    private fun initializeBinding() {
        remindersListViewModel = RemindersListViewModel()
        binding.viewModel = remindersListViewModel
    }

    override fun update(p0: Observable?, p1: Any?) {
        when (p0) {
            is RemindersListViewModel -> {
                val adapter = binding.recyclerview.adapter as RemindersListAdapter
                adapter.setRemindersList(p0.remindersList)
            }
        }
    }

    companion object {
        fun newInstance(): RemindersListFragment = RemindersListFragment()
    }
}// Required empty public constructor
