package com.amatrus.codeforcesbrowser.reminders.list

import android.view.View
import android.widget.Toast
import com.amatrus.codeforcesbrowser.data.database.App
import com.amatrus.codeforcesbrowser.data.database.DatabaseRepository


/**
 * Created by engnaruto on 12/20/17.
 */


class ReminderItemViewModel(private val reminder: Reminder) {

    private val PROBLEM_NAME_CHARACTER_COUNT = 100

    fun contestName(): String {
        if (reminder.contestName.length > PROBLEM_NAME_CHARACTER_COUNT) {
            return "${reminder.contestName.substring(0, PROBLEM_NAME_CHARACTER_COUNT)}..."
        }
        return reminder.contestName
    }

    fun onClickDeleteReminderButton(view: View) {
        deleteReminder()
        Toast.makeText(view.context, "${reminder.contestName} reminder has been removed", Toast.LENGTH_SHORT).show()
    }

    private fun deleteReminder() {
        val databaseRepository = DatabaseRepository()
        databaseRepository.removeReminder(reminder.id)
        App.dispatcher.cancel(reminder.tag)
    }
}