package com.amatrus.codeforcesbrowser.reminders.list


import android.util.Log
import com.amatrus.codeforcesbrowser.base.BaseListViewModel

/**
 * Created by engnaruto on 12/20/17.
 */


class RemindersListViewModel : BaseListViewModel() {

    lateinit var remindersList: List<Reminder>

    private val NO_REMINDERS_TO_SHOW_WARNING = "No reminders to show"

    fun getRemindersList() {
        val remindersList = getDataFromDatabase()
        Log.e("REMINDERS LIST", remindersList.toString())
        if (!remindersList.isEmpty()) {
            setNewRemindersList(remindersList)
            showResults()
        } else {
            showErrorMessage(NO_REMINDERS_TO_SHOW_WARNING)
        }
    }

    private fun getDataFromDatabase(): List<Reminder> {
        return databaseRepository.getAllReminders()
    }

    private fun setNewRemindersList(resultsList: List<Reminder>) {
        remindersList = resultsList
        setChanged()
        notifyObservers()
    }
}