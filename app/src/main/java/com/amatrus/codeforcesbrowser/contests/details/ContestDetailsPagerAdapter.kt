package com.amatrus.codeforcesbrowser.contests.details

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.amatrus.codeforcesbrowser.contests.list.Contest
import com.amatrus.codeforcesbrowser.contests.list.ContestsListFragment

/**
 * Created by engnaruto on 12/9/17.
 */

class ContestDetailsPagerAdapter(fragmentManager: FragmentManager, val contest: Contest) : FragmentPagerAdapter(fragmentManager) {
    private val NUMBER_OF_FRAGMENTS = 2

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ContestDetailsInfoFragment.newInstance(contest)
            1 -> ContestDetailsProblemsListFragment.newInstance(contest.id)
            else -> ContestsListFragment.newInstance(false)
        }
    }

    override fun getCount(): Int {
        return NUMBER_OF_FRAGMENTS
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Info"
            1 -> "Problems"
            else -> "Not Available"
        }
    }
}