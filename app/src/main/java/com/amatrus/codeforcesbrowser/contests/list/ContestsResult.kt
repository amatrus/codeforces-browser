package com.amatrus.codeforcesbrowser.contests.list


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

/**
 * Created by engnaruto on 11/2/17.
 */

data class ContestsResult(
        @SerializedName("status") val status: String, //OK
        @SerializedName("result") val result: List<Contest>
)

@Entity
data class Contest(
        @Id var cid: Long = 0, //100001
        var isGym: Boolean = false, //added before objects added to the database
        @SerializedName("id") val id: Int = -1, //100001
        @SerializedName("name") val name: String = "", //2010 Codeforces Beta Round #1 (training)
        @SerializedName("type") val type: String = "", //ICPC
        @SerializedName("phase") var phase: String = "", //FINISHED
        @SerializedName("frozen") val frozen: Boolean = false, //false
        @SerializedName("durationSeconds") val durationSeconds: Int = -1, //18000
        @SerializedName("startTimeSeconds") val startTimeSeconds: Int = -1, //1507983000
        @SerializedName("relativeTimeSeconds") val relativeTimeSeconds: Int = -1, //1846888
        @SerializedName("preparedBy") val preparedBy: String = "", //MikeMirzayanov
        @SerializedName("websiteUrl") val websiteUrl: String = "", //http://neerc.ifmo.ru/school/io/
        @SerializedName("description") val description: String = "", //This is the only contest for testing Codeforces::Gym. As you participate in any other training, you guarantee that you solve problems without assistance and that you do not send other people's solutions.
        @SerializedName("difficulty") val difficulty: Int = -1, //3
        @SerializedName("kind") val kind: String = "", //Official ACM-ICPC Contest
        @SerializedName("icpcRegion") val icpcRegion: String = "", //Northeastern Europe Region
        @SerializedName("country") val country: String = "", //Russia
        @SerializedName("city") val city: String = "", //Moscow
        @SerializedName("season") val season: String = ""//2010-2011
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readLong(),
            1 == source.readInt(),
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readString(),
            1 == source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeLong(cid)
        writeInt((if (isGym) 1 else 0))
        writeInt(id)
        writeString(name)
        writeString(type)
        writeString(phase)
        writeInt((if (frozen) 1 else 0))
        writeInt(durationSeconds)
        writeInt(startTimeSeconds)
        writeInt(relativeTimeSeconds)
        writeString(preparedBy)
        writeString(websiteUrl)
        writeString(description)
        writeInt(difficulty)
        writeString(kind)
        writeString(icpcRegion)
        writeString(country)
        writeString(city)
        writeString(season)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Contest> = object : Parcelable.Creator<Contest> {
            override fun createFromParcel(source: Parcel): Contest = Contest(source)
            override fun newArray(size: Int): Array<Contest?> = arrayOfNulls(size)
        }
    }
}