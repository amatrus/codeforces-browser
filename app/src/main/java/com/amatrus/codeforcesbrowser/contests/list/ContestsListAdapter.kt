package com.amatrus.codeforcesbrowser.contests.list


import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.databinding.ContestItemBinding

/**
 * Created by engnaruto on 11/2/17.
 */

class ContestsListAdapter : RecyclerView.Adapter<ContestsListAdapter.ContestItemViewHolder>() {

    private var contestsList: List<Contest> = listOf()


    fun setContestsList(list:List<Contest>){
        contestsList = list
        notifyDataSetChanged()
    }
    override fun onBindViewHolder(holder: ContestItemViewHolder, position: Int) {
        holder.bind(contestsList[position])
    }

    override fun getItemCount(): Int {
        return contestsList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContestItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ContestItemBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.contest_item, parent, false)

        return ContestItemViewHolder(binding)
    }


    class ContestItemViewHolder(private val binding: ContestItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Contest) {
            binding.viewModel = ContestItemViewModel(data)
        }
    }
}

