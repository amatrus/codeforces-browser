package com.amatrus.codeforcesbrowser.contests.parent

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.amatrus.codeforcesbrowser.contests.list.ContestsListFragment


class ContestsParentPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
    private val NUMBER_OF_FRAGMENTS = 2


    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ContestsListFragment.newInstance(false)
            1 -> ContestsListFragment.newInstance(true)
            else -> ContestsListFragment.newInstance(false)
        }
    }

    override fun getCount(): Int {
        return NUMBER_OF_FRAGMENTS
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Regular Contests"
            1 -> "Gym Contests"
            else -> "Not Available"
        }
    }
}