package com.amatrus.codeforcesbrowser.contests.details

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.base.BaseListFragment
import com.amatrus.codeforcesbrowser.databinding.ContestDetailsProblemsListFragmentBinding
import com.amatrus.codeforcesbrowser.problemset.problems.list.ProblemsListAdapter
import java.util.*

/**
 * Created by engnaruto on 12/9/17.
 */

class ContestDetailsProblemsListFragment : BaseListFragment() {
    private lateinit var binding: ContestDetailsProblemsListFragmentBinding
    private lateinit var ContestDetailsProblemsListViewModel: ContestDetailsProblemsListViewModel

    private var contestId: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            contestId = arguments!!.getInt(CONTEST_ID)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.contest_details_problems_list_fragment, container, false)
        initializeBinding()
        setUpRecyclerView(binding.recyclerviewProblems, 1, 10, ProblemsListAdapter())
        setupObserver(ContestDetailsProblemsListViewModel)
        ContestDetailsProblemsListViewModel.getProblemsList()
        return binding.root
    }

    private fun initializeBinding() {
        ContestDetailsProblemsListViewModel = ContestDetailsProblemsListViewModel(contestId)
        binding.viewModel = ContestDetailsProblemsListViewModel
    }

    override fun update(p0: Observable?, p1: Any?) {
        when (p0) {
            is ContestDetailsProblemsListViewModel -> {
                val adapter = binding.recyclerviewProblems.adapter as ProblemsListAdapter
                adapter.setProblemsList(p0.problemsList)
            }
        }
    }

    companion object {
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val CONTEST_ID = "CONTEST_ID"

        fun newInstance(contestId: Int): ContestDetailsProblemsListFragment {
            val fragment = ContestDetailsProblemsListFragment()
            val args = Bundle()
            args.putInt(CONTEST_ID, contestId)
            fragment.arguments = args

            return fragment
        }
    }
}// Required empty public constructor