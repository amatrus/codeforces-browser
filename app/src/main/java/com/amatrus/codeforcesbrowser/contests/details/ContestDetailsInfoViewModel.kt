package com.amatrus.codeforcesbrowser.contests.details

import android.databinding.ObservableInt
import android.view.View
import com.amatrus.codeforcesbrowser.base.BaseContestViewModel
import com.amatrus.codeforcesbrowser.contests.list.Contest

/**
 * Created by engnaruto on 11/9/17.
 */


data class ContestDetailsInfoViewModel(override val contest: Contest) : BaseContestViewModel(contest) {

    var descriptionRowVisibility: ObservableInt = ObservableInt(View.GONE)
    var typeRowVisibility: ObservableInt = ObservableInt(View.GONE)
    var icpcRegionRowVisibility: ObservableInt = ObservableInt(View.GONE)
    var homeRowVisibility: ObservableInt = ObservableInt(View.GONE)
    var seasonRowVisibility: ObservableInt = ObservableInt(View.GONE)
    var preparedByRowVisibility: ObservableInt = ObservableInt(View.GONE)
    var kindRowVisibility: ObservableInt = ObservableInt(View.GONE)
    var difficultyRowVisibility: ObservableInt = ObservableInt(View.GONE)
    var frozenRowVisibility: ObservableInt = ObservableInt(View.GONE)

    fun contestDescription(): String {
        if (contest.description != "") {
            descriptionRowVisibility.set(View.VISIBLE)
        }
        return contest.description
    }

    fun contestType(): String {
        if (contest.type != "") {
            typeRowVisibility.set(View.VISIBLE)
        }
        return contest.type
    }

    fun contestIcpcRegion(): String {
        if (contest.icpcRegion != "") {
            icpcRegionRowVisibility.set(View.VISIBLE)
        }
        return contest.icpcRegion
    }

    fun contestHome(): String {
        if (contest.city != "" && contest.country != "") {
            homeRowVisibility.set(View.VISIBLE)
        }

        if (contest.country == "") {
            return contest.city
        } else if (contest.city == "") {
            return contest.country
        }
        return "${contest.city}, ${contest.country}"
    }

    fun contestSeason(): String {
        if (contest.season != "") {
            seasonRowVisibility.set(View.VISIBLE)
        }
        return contest.season
    }

    fun contestPreparedBy(): String {
        if (contest.preparedBy != "") {
            preparedByRowVisibility.set(View.VISIBLE)
        }
        return contest.preparedBy
    }

    fun contestKind(): String {
        if (contest.kind != "") {
            kindRowVisibility.set(View.VISIBLE)
        }
        return contest.kind
    }

    fun contestDifficulty(): String {
        if (contest.difficulty != -1) {
            difficultyRowVisibility.set(View.VISIBLE)
        }
        return contest.difficulty.toString()
    }

    fun contestFrozen(): String {
        if (contest.frozen) {
            frozenRowVisibility.set(View.VISIBLE)
        }
        return if (contest.frozen) {
            "YES"
        } else {
            "NO"
        }
    }

    fun contestDurationTime(): String {
        return "${contestDuration()} ${contestDurationUnit()}"
    }

    fun contestStartTimeWithUnit(): String {
        return "${contestStartTime()} (${userTimeZone()})"
    }
}