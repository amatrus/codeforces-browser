package com.amatrus.codeforcesbrowser.contests.details

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.contests.list.Contest
import com.amatrus.codeforcesbrowser.databinding.ContestDetailsActivityBinding
import org.parceler.Parcels

/**
 * Created by engnaruto on 12/9/17.
 */

class ContestDetailsActivity : AppCompatActivity() {

    val title = "Contest Details"
    private val CONTEST_EXTRA = "CONTEST_EXTRA"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ContestDetailsActivityBinding = DataBindingUtil.setContentView(this, R.layout.contest_details_activity)

        val tabLayout = binding.slidingTabs
        val viewPager = binding.contestsViewPager
        val toolbar = binding.toolbar

        val contest = Parcels.unwrap<Contest>(intent.getParcelableExtra(CONTEST_EXTRA))

        val contestPagerAdapter = ContestDetailsPagerAdapter(supportFragmentManager, contest)

        initializeTabLayout(tabLayout, viewPager, contestPagerAdapter)
        initializeToolBar(toolbar, title)
    }

    private fun initializeTabLayout(tabLayout: TabLayout, viewPager: ViewPager, fragmentPagerAdapter: FragmentPagerAdapter) {
        viewPager.adapter = fragmentPagerAdapter
        tabLayout.setupWithViewPager(viewPager)
    }

    private fun initializeToolBar(toolbar: Toolbar, title: String) {
        setSupportActionBar(toolbar)
        setTitle(title)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }
}