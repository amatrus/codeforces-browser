package com.amatrus.codeforcesbrowser.contests.details

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.contests.list.Contest
import com.amatrus.codeforcesbrowser.databinding.ContestDetailsInfoFragmentBinding

class ContestDetailsInfoFragment : Fragment() {
    lateinit var binding: ContestDetailsInfoFragmentBinding
    lateinit var contestDetailsInfoViewModel: ContestDetailsInfoViewModel


    private lateinit var contest: Contest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            contest = arguments!!.getParcelable(CONTEST)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.contest_details_info_fragment, container, false)

        initializeBinding(contest)
        return binding.root
    }

    private fun initializeBinding(contest: Contest) {
        contestDetailsInfoViewModel = ContestDetailsInfoViewModel(contest)
        binding.viewModel = contestDetailsInfoViewModel
    }

    companion object {
        val CONTEST = "CONTEST"

        fun newInstance(contest: Contest): ContestDetailsInfoFragment {

            val fragment = ContestDetailsInfoFragment()
            val args = Bundle()
            args.putParcelable(CONTEST, contest)
            fragment.arguments = args

            return fragment
        }
    }

}// Required empty public constructor