package com.amatrus.codeforcesbrowser.contests.details

import com.amatrus.codeforcesbrowser.base.BaseListViewModel
import com.amatrus.codeforcesbrowser.problemset.problems.list.Problem

/**
 * Created by engnaruto on 12/9/17.
 */


class ContestDetailsProblemsListViewModel(val contestId: Int) : BaseListViewModel() {

    lateinit var problemsList: List<Problem>

    private fun getDataFromDatabase(): List<Problem> {
        return databaseRepository.getProblemsOfContest(contestId.toLong())
    }

    fun getProblemsList() {
        processResults(getDataFromDatabase())
    }

    private fun processResults(result: List<Problem>) {
        setNewContestsList(result)
        showResults()
    }

    private fun setNewContestsList(resultsList: List<Problem>) {
        problemsList = resultsList
        setChanged()
        notifyObservers()
    }
}