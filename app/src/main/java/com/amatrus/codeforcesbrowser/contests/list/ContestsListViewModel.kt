package com.amatrus.codeforcesbrowser.contests.list

import android.util.Log
import android.widget.Toast
import com.amatrus.codeforcesbrowser.base.BaseListViewModel
import com.amatrus.codeforcesbrowser.data.database.App
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by naruto on 11/11/17.
 */
class ContestsListViewModel(private val isGym: Boolean) : BaseListViewModel() {

    lateinit var contestsList: List<Contest>

    fun getContestsList() {
        if (App.isOnline()) {
            getDataFromApi()
        } else {
            Toast.makeText(App.app, NO_INTERNET_CONNECTION_ERROR_MESSAGE, Toast.LENGTH_SHORT).show()
            setNewContestsList(getDataFromDatabase())
            showResults()
        }
    }

    private fun getDataFromDatabase(): List<Contest> {
        return databaseRepository.getAllContests(isGym)
    }

    private fun removeNotFinishedContestsFromDatabase(contestCids: List<Long>) {
        databaseRepository.removeContests(contestCids)
    }

    private fun getDataFromApi() {
        repository.getContestsList(isGym)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    processResults(result)
                }, { error ->
                    error.printStackTrace()
                    setNewContestsList(getDataFromDatabase())
                    showResults()
                    Toast.makeText(App.app, LOADING_DATA_FROM_SERVER_ERROR_MESSAGE, Toast.LENGTH_SHORT).show()
                })
    }

    private fun processResults(result: ContestsResult) {
        if (result.status == "OK") {
            Log.e("Result", "There are ${result.result.size} Contests - $isGym")

            var newContests = result.result.sortedByDescending { it.startTimeSeconds }
            val numberOfContests = databaseRepository.getNumberOfContests(isGym)
            Log.e("numberOfContests", "isGym: $isGym => $numberOfContests")

            if (numberOfContests != 0) {
                val databaseContestsList = getDataFromDatabase()

                var firstFinishedContestIndex = 0
                val contestCids = mutableListOf<Long>()

                while (databaseContestsList[firstFinishedContestIndex].phase != "FINISHED") {
                    contestCids.add(databaseContestsList[firstFinishedContestIndex++].cid)
                }

                Log.e("firstFinishedContest", "$firstFinishedContestIndex - ${databaseContestsList[firstFinishedContestIndex].name}")

                var firstNewContestIndex = 0

                while (newContests[firstNewContestIndex].id != databaseContestsList[firstFinishedContestIndex].id) {
                    firstNewContestIndex++
                }

                removeNotFinishedContestsFromDatabase(contestCids)

                if (firstNewContestIndex != 0) {
                    newContests = newContests.slice(IntRange(0, firstNewContestIndex - 1))
                    databaseRepository.addContests(newContests, isGym)
                    newContests.forEach { Log.e("ADDED1 $firstNewContestIndex", it.toString()) }
                }

//                var i = 0
//
//                while (newContests[i].id != databaseContestsList[0].id) {
//                    i++
//                }
//
//                var x = 0
//                var ii = i
//                while (databaseContestsList[x].phase != "FINISHED") {
//                    if (newContests[ii].id == databaseContestsList[x].id
//                            && newContests[ii].phase != databaseContestsList[x].phase) {
//                        databaseContestsList[x].phase = newContests[ii].phase
//                        databaseRepository.updateContest(databaseContestsList[x])
//                    } else if (newContests[ii].name == databaseContestsList[x].name
//                            && newContests[ii].id != databaseContestsList[x].id) {
//                        databaseContestsList[x].id = newContests[ii].id
//                        databaseRepository.updateContest(databaseContestsList[x])
//                    }
//                    ii++
//                    x++
//                }
//
//                if (i != 0) {
//                    newContests = newContests.slice(IntRange(0, i))
//                    databaseRepository.addContests(newContests, isGym)
//                    newContests.forEach { Log.e("ADDED1 $i", it.toString()) }
//                }
            } else {
                databaseRepository.addContests(newContests, isGym)
                newContests.forEach { Log.e("**ADDED2**", it.toString()) }
            }
        } else {
            showErrorMessage(LOADING_DATA_FROM_SERVER_ERROR_MESSAGE)
        }

        val contestsList = getDataFromDatabase()
        setNewContestsList(contestsList)
        showResults()
    }

    private fun setNewContestsList(resultsList: List<Contest>) {
        contestsList = resultsList
        setChanged()
        notifyObservers()
    }
}