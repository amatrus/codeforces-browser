package com.amatrus.codeforcesbrowser.contests.list

import android.content.Context
import android.content.Intent
import android.databinding.ObservableInt
import android.view.View
import android.widget.Toast
import com.amatrus.codeforcesbrowser.base.BaseContestViewModel
import com.amatrus.codeforcesbrowser.contests.details.ContestDetailsActivity
import com.amatrus.codeforcesbrowser.data.database.DatabaseRepository
import com.amatrus.codeforcesbrowser.reminders.list.Reminder
import com.amatrus.codeforcesbrowser.reminders.notifications.NotificationReminder
import org.parceler.Parcels


/**
 * Created by naruto on 11/4/17.
 */

data class ContestItemViewModel(override val contest: Contest) : BaseContestViewModel(contest) {
    private val CONTEST_REGISTRATION_LINK = "http://codeforces.com/contestRegistration/${contest.id}"
    private val SHARING_MESSAGE = "Don't forget to register at ${contest.name} contest @codeforces before ${contestStartDate()} ${contestStartTime()} (${userTimeZone()}) $CONTEST_REGISTRATION_LINK"
    private val NOT_FINISHED_CONTEST_MESSAGE = "You can view the problems for the finished contests only"
    private val CONTEST_EXTRA = "CONTEST_EXTRA"

    var actionBarVisibility: ObservableInt = ObservableInt(View.GONE)
    var timeTableVisibility: ObservableInt = ObservableInt(View.GONE)

    init {
        if (contest.phase == "BEFORE") {
            timeTableVisibility.set(View.VISIBLE)
            actionBarVisibility.set(View.VISIBLE)
        } else {
            actionBarVisibility.set(View.GONE)
            timeTableVisibility.set(View.GONE)
        }
    }

    fun onClickRegularContestItem(view: View) {
        if (contest.phase == "FINISHED") {
            val intent = Intent(view.context, ContestDetailsActivity::class.java)
            intent.putExtra(CONTEST_EXTRA, Parcels.wrap(contest))
            view.context.startActivity(intent)
        } else {
            Toast.makeText(view.context, NOT_FINISHED_CONTEST_MESSAGE, Toast.LENGTH_SHORT).show()
        }
    }

    fun onClickReminderButton(view: View) {
        val newReminder = Reminder(0, contest.id, contest.name, contest.startTimeSeconds, "reminder-${contest.id}-${System.currentTimeMillis()}")
        val isCreated = createNotificationReminder(newReminder)
        if (isCreated) {
            addReminderToDatabase(newReminder)
            Toast.makeText(view.context, "A reminder for ${contest.name} contest has been created", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(view.context, "A reminder for ${contest.name} contest has not been created because the contest will start soon", Toast.LENGTH_SHORT).show()
        }
    }

    private fun addReminderToDatabase(newReminder: Reminder) {
        val databaseRepository = DatabaseRepository.create()
        databaseRepository.addReminder(newReminder)
    }

    private fun createNotificationReminder(newReminder: Reminder): Boolean {
        return NotificationReminder(newReminder).create()
    }

    fun onClickShareButton(view: View) {
        shareContest(view.context)
    }

    private fun shareContest(context: Context) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, SHARING_MESSAGE)
        context.startActivity(Intent.createChooser(intent, "Share via"))
    }
}