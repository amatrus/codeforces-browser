package com.amatrus.codeforcesbrowser.contests.parent

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.databinding.ContestsParentFragmentBinding
import com.amatrus.codeforcesbrowser.base.BaseParentFragment

/**
 * Created by engnaruto on 11/1/17.
 */

class ContestsParentFragment : BaseParentFragment() {

    val title = "Contests"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding: ContestsParentFragmentBinding = DataBindingUtil
                .inflate(inflater, R.layout.contests_parent_fragment, container, false)

        val tabLayout = binding.slidingTabs
        val viewPager = binding.contestsViewPager
        val toolbar = binding.toolbar

        val contestPagerAdapter = ContestsParentPagerAdapter(childFragmentManager)

        initializeTabLayout(tabLayout, viewPager, contestPagerAdapter)
        initializeToolBar(toolbar, title)

        return binding.root
    }

    companion object {
        fun newInstance(): ContestsParentFragment = ContestsParentFragment()
    }
}// Required empty public constructor