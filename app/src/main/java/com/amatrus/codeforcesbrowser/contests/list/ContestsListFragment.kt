package com.amatrus.codeforcesbrowser.contests.list

import android.content.SharedPreferences
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.base.BaseListFragment
import com.amatrus.codeforcesbrowser.data.database.App
import com.amatrus.codeforcesbrowser.databinding.ContestsListFragmentBinding
import java.util.*


/**
 * Created by engnaruto on 11/1/17.
 */

class ContestsListFragment : BaseListFragment(), SharedPreferences.OnSharedPreferenceChangeListener {

    private lateinit var binding: ContestsListFragmentBinding
    private lateinit var contestsListViewModel: ContestsListViewModel

    private var isGym: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            isGym = arguments!!.getBoolean(IS_GYM)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.contests_list_fragment, container, false)
        initializeBinding()
        setUpRecyclerView(binding.recyclerviewRegularContests, 1, 10, ContestsListAdapter())
        setupObserver(contestsListViewModel)
        contestsListViewModel.getContestsList()
        initializeSharedPreferences()
        return binding.root
    }

    private fun initializeBinding() {
        contestsListViewModel = ContestsListViewModel(isGym)
        binding.viewModel = contestsListViewModel
    }

    private fun initializeSharedPreferences() {
        val sharedPreferences = App.sharedPreferences
        // Register the listener
        sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == getString(R.string.pref_show_upcoming_contests_only)) {
            Log.e("LISTENER", "key = $key") // TODO: Show UPCOMING Contests only
            // FIXME: Fix when the contest is FINISHED and still shown as UPCOMING
        }
    }

    override fun update(p0: Observable?, p1: Any?) {
        when (p0) {
            is ContestsListViewModel -> {
                val adapter = binding.recyclerviewRegularContests.adapter as ContestsListAdapter
                adapter.setContestsList(p0.contestsList)
            }
        }
    }

    companion object {
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val IS_GYM = "CONTEST_TYPE"

        fun newInstance(isGym: Boolean): ContestsListFragment {
            val fragment = ContestsListFragment()
            val args = Bundle()
            args.putBoolean(IS_GYM, isGym)
            fragment.arguments = args

            return fragment
        }
    }
}// Required empty public constructor