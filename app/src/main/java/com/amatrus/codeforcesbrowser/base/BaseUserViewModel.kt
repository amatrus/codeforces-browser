package com.amatrus.codeforcesbrowser.base

import android.databinding.BindingAdapter
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.user.info.User
import com.amatrus.codeforcesbrowser.user.info.UserResult
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by engnaruto on 11/28/17.
 */


open class BaseUserViewModel : BaseViewModel() {

    var user = User()

    fun getUser(handle: String) {
        repository.getUser(handle)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    processResults(result)
                }, { error ->
                    error.printStackTrace()
                    showErrorMessage(error.message.toString())
                })
    }

    private fun processResults(result: UserResult) {
        if (result.status == "OK") {
            Log.e("Navigation Result", "There are ${result.result.size} User")
            setNewUser(result.result[0])
            Log.e("Navigation Result", user.toString())
            showResults()
        } else {
            showErrorMessage(LOADING_DATA_FROM_SERVER_ERROR_MESSAGE)
        }
    }

    private fun setNewUser(resultUser: User) {
        user = resultUser
        setChanged()
        notifyObservers()
    }

    //    @SerializedName("handle") val handle: String = "", //engnaruto
    fun handle(): String {
        if (user.handle == "") {
            return "New User"
        }
        return user.handle
    }

    //    @SerializedName("currentRank") val currentRank: String = "", //pupil
    fun currentRank(): String = user.rank.capitalize()
            .split(" ").joinToString(" ") { it.capitalize() }

    //    @SerializedName("currentRating") val currentRating: Int = 0, //1289
    fun currentRating(): String {
        if (user.rating == 0) {
            return ""
        }
        return user.rating.toString()
    }

    object TextViewBindingAdapter {

        /*
        Ranking Colors
        ---------------------------------------------
        Newbie				        Gray	#808080
        Pupil				        Green	#008000
        Specialist			        Cyan	#03a89e
        Expert				        Blue	#0000ff
        Candidate Master		    Violet	#a0a
        Master				        Orange	#ff8c00
        International Master		Orange	#ff8c00
        Grandmaster			        Red	    #ff0000
        International Grandmaster	Red	    #ff0000
        Legendary Grandmaster		Red	    #ff0000	=> First Letter is Black
    */

        private val userColorMap = mapOf(
                Pair("newbie", R.color.userColorGray),
                Pair("pupil", R.color.userColorGreen),
                Pair("specialist", R.color.userColorCyan),
                Pair("expert", R.color.userColorBlue),
                Pair("candidate master", R.color.userColorViolet),
                Pair("master", R.color.userColorOrange),
                Pair("international master", R.color.userColorOrange),
                Pair("grandmaster", R.color.userColorRed),
                Pair("international grandmaster", R.color.userColorRed),
                Pair("legendary grandmaster", R.color.userColorRed)
        )

        private fun getUserColor(rank: String): Int =
                userColorMap[rank.toLowerCase()] ?: R.color.userColorBlack

        @BindingAdapter("color")
        @JvmStatic
        fun setFont(textView: TextView, rank: String) {
            textView.setTextColor(
                    ContextCompat.getColor(textView.context, getUserColor(rank.toLowerCase()))
            )
        }
    }

    //    @SerializedName("titlePhoto") val titlePhoto: String = "" //http://userpic.codeforces.com/74645/title/2c519b699b3e7235.jpg
    fun userImage(): String = user.titlePhoto

    object ImageViewBindingAdapter {
        @BindingAdapter("imageUrl")
        @JvmStatic
        fun loadImage(view: ImageView, imageUrl: String) {
            Picasso.get()
                    .load("http:$imageUrl")
                    .placeholder(R.drawable.ic_menu_user)
                    .error(R.drawable.ic_menu_user)
                    .into(view)
        }
    }
}