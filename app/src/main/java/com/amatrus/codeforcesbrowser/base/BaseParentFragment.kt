package com.amatrus.codeforcesbrowser.base

import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import com.amatrus.codeforcesbrowser.R

/**
 * Created by engnaruto on 11/26/17.
 */

open class BaseParentFragment : Fragment() {


    open fun initializeTabLayout(tabLayout: TabLayout, viewPager: ViewPager, fragmentPagerAdapter: FragmentPagerAdapter) {
        viewPager.adapter = fragmentPagerAdapter
        tabLayout.setupWithViewPager(viewPager)
    }

    open fun initializeToolBar(toolbar: Toolbar, title: String) {
        val activity = activity as AppCompatActivity

        activity.setSupportActionBar(toolbar)
        activity.title = title

        val drawer = activity.findViewById<View>(R.id.drawer_layout) as DrawerLayout

        val toggle = ActionBarDrawerToggle(
                activity, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)

        drawer.addDrawerListener(toggle)

        toggle.syncState()

        toolbar.setNavigationOnClickListener({
            //open navigation drawer when click navigation back button
            drawer.openDrawer(GravityCompat.START)
        })
    }
}