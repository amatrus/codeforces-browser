package com.amatrus.codeforcesbrowser.base

import android.databinding.ObservableInt
import android.view.View
import com.amatrus.codeforcesbrowser.data.database.DatabaseRepository

/**
 * Created by engnaruto on 11/26/17.
 */

open class BaseListViewModel : BaseViewModel() {

    val databaseRepository = DatabaseRepository.create()

    var recyclerViewVisibility: ObservableInt = ObservableInt(View.VISIBLE)

}