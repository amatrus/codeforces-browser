package com.amatrus.codeforcesbrowser.base

import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.view.View
import com.amatrus.codeforcesbrowser.data.api.SearchRepositoryProvider
import java.util.*

/**
 * Created by engnaruto on 11/26/17.
 */

open class BaseViewModel : Observable() {

    var progressBarVisibility: ObservableInt = ObservableInt(View.VISIBLE)
    var textViewErrorVisibility: ObservableInt = ObservableInt(View.GONE)
    var textViewErrorText: ObservableField<String> = ObservableField()

    val repository = SearchRepositoryProvider.provideSearchRepository()

    val LOADING_DATA_FROM_SERVER_ERROR_MESSAGE = "Failed to load data from the server, please try again later :("
    val NO_INTERNET_CONNECTION_ERROR_MESSAGE = "No internet connection, please try again :("

    open fun showResults() {
        progressBarVisibility.set(View.GONE)
        textViewErrorVisibility.set(View.GONE)
    }

    open fun showErrorMessage(errorMessage: String) {
        progressBarVisibility.set(View.GONE)

        textViewErrorVisibility.set(View.VISIBLE)
        textViewErrorText.set(errorMessage)
    }
}