package com.amatrus.codeforcesbrowser.base

import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import java.util.*

/**
 * Created by engnaruto on 11/26/17.
 */

open class BaseListFragment : Fragment(), Observer {

    open fun setUpRecyclerView(
        recyclerView: RecyclerView,
        spanCount: Int,
        gapSize: Int,
        listAdapter: RecyclerView.Adapter<*>
    ) {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addItemDecoration(SpacingItemDecoration(spanCount, dpToPx(gapSize), true))
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = listAdapter
    }

    /**
     * Converting dp to pixel
     */
    private fun dpToPx(dp: Int): Int {
        val r = resources
        return Math.round(
            TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp.toFloat(),
                r.displayMetrics
            )
        )
    }

    open fun setupObserver(observable: Observable) {
        observable.addObserver(this)
    }

    override fun update(p0: Observable?, p1: Any?) {

    }

    open fun beginSearch(query: String) {

    }
}