package com.amatrus.codeforcesbrowser.base

import android.databinding.BindingAdapter
import android.support.v4.content.ContextCompat
import android.widget.TextView
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.contests.list.Contest
import com.amatrus.codeforcesbrowser.extensions.convertDurationTimeToHours
import com.amatrus.codeforcesbrowser.extensions.convertTimeSecondsToDate

/**
 * Created by engnaruto on 12/19/17.
 */

open class BaseContestViewModel(open val contest: Contest) {

    fun contestName(): String {
        return contest.name
    }

    fun contestPhase(): String {
        if (contest.phase == "BEFORE") {
            return "UPCOMING"
        } else if (contest.phase == "CODING") {
            return "ONGOING"
        }
        return contest.phase
    }

    fun contestStartTime(): String {
        val timeFormat = "HH:mm"
        return contest.startTimeSeconds.convertTimeSecondsToDate(timeFormat)
    }

    fun contestDuration(): String {
        return contest.durationSeconds.convertDurationTimeToHours()
    }

    fun contestStartDate(): String {
        val dateFormat = "dd/MMM/yyy"
        return contest.startTimeSeconds.convertTimeSecondsToDate(dateFormat)
    }

    fun contestDurationUnit(): String {
        if (contestDuration() == "01:00") {
            return "Hour"
        }
        return "Hours"
    }

    fun userTimeZone(): String {
        val dateFormat = "z"
        return contest.startTimeSeconds.convertTimeSecondsToDate(dateFormat)
    }

    object TextViewBindingAdapter {

        /*
            UPCOMING                Green
            ONGOING                 Blue
            PENDING_SYSTEM_TEST     Gray
            SYSTEM_TEST             Orange
            FINISHED                Red
        */

        private val contestPhaseColorMap = mapOf(
                Pair("UPCOMING", R.color.userColorGreen),
                Pair("ONGOING", R.color.userColorBlue),
                Pair("PENDING_SYSTEM_TEST", R.color.userColorGray),
                Pair("SYSTEM_TEST", R.color.userColorOrange),
                Pair("FINISHED", R.color.userColorRed)
        )

        private fun getContestPhaseColor(phase: String): Int =
                contestPhaseColorMap[phase] ?: R.color.userColorBlack

        @BindingAdapter("phaseColor")
        @JvmStatic
        fun setFont(textView: TextView, phase: String) {
            textView.setTextColor(
                    ContextCompat.getColor(textView.context, getContestPhaseColor(phase))
            )
        }
    }
}