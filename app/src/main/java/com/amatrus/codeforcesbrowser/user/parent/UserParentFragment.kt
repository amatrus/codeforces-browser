package com.amatrus.codeforcesbrowser.user.parent


import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.amatrus.codeforcesbrowser.base.BaseParentFragment
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.databinding.UserParentFragmentBinding


/**
 * Created by engnaruto on 11/19/17.
 */

class UserParentFragment : BaseParentFragment() {

    val title = "User Info"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding: UserParentFragmentBinding = DataBindingUtil
                .inflate(inflater, R.layout.user_parent_fragment, container, false)

        val tabLayout = binding.slidingTabs
        val viewPager = binding.userViewPager
        val toolbar = binding.toolbar

        val userPagerAdapter = UserParentPagerAdapter(childFragmentManager)

        initializeTabLayout(tabLayout, viewPager, userPagerAdapter)
        initializeToolBar(toolbar, title)

        return binding.root
    }

    companion object {
        fun newInstance(): UserParentFragment = UserParentFragment()
    }

}// Required empty public constructor
