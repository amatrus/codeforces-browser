package com.amatrus.codeforcesbrowser.user.parent

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.amatrus.codeforcesbrowser.user.info.UserInfoFragment

/**
* Created by engnaruto on 11/18/17.
*/
class UserParentPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
//    private val NUMBER_OF_FRAGMENTS = 4
    private val NUMBER_OF_FRAGMENTS = 1


    override fun getItem(position: Int): Fragment = when (position) {
        0 -> UserInfoFragment.newInstance()
//        1 ->UserInfoFragment.newInstance()
        else -> UserInfoFragment.newInstance()
    }

    override fun getCount(): Int = NUMBER_OF_FRAGMENTS

    override fun getPageTitle(position: Int): CharSequence? = when (position) {
        0 -> "Info"
//        1 -> "Friends"
//        2 -> "Submissions"
//        3 -> "Blog"
        else -> "N/A"
    }
}