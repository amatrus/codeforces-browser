package com.amatrus.codeforcesbrowser.user.info

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


/**
 * Created by naruto on 11/18/17.
 */

data class UserResult(
        @SerializedName("status") val status: String = "", //OK
        @SerializedName("result") val result: List<User> = listOf()
)


data class User(
        @SerializedName("handle") val handle: String = "", //engnaruto
        @SerializedName("email") val email: String = "", //tarekcsed@yahoo.com
        @SerializedName("vkId") val vkId: String = "", //UNKNOWN
        @SerializedName("openId") val openId: String = "", //UNKNOWN
        @SerializedName("firstName") val firstName: String = "", //Tarek
        @SerializedName("lastName") val lastName: String = "", //Khaled
        @SerializedName("country") val country: String = "", //Egypt
        @SerializedName("city") val city: String = "", //Alexandria
        @SerializedName("organization") val organization: String = "", //University of Alexandria, Faculty of Engineering
        @SerializedName("contribution") val contribution: Int = 0, //0
        @SerializedName("rank") val rank: String = "", //pupil
        @SerializedName("rating") val rating: Int = 0, //1289
        @SerializedName("maxRank") val maxRank: String = "", //specialist
        @SerializedName("maxRating") val maxRating: Int = 0, //1411
        @SerializedName("lastOnlineTimeSeconds") val lastOnlineTimeSeconds: Int = 0, //1511112909
        @SerializedName("registrationTimeSeconds") val registrationTimeSeconds: Int = 0, //1353279110
        @SerializedName("friendOfCount") val friendOfCount: Int = 0, //34
        @SerializedName("avatar") val avatar: String = "null", //http://userpic.codeforces.com/74645/avatar/64e9480ceb3bba47.jpg
        @SerializedName("titlePhoto") val titlePhoto: String = "null" //http://userpic.codeforces.com/74645/title/2c519b699b3e7235.jpg
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readInt(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(handle)
        writeString(email)
        writeString(vkId)
        writeString(openId)
        writeString(firstName)
        writeString(lastName)
        writeString(country)
        writeString(city)
        writeString(organization)
        writeInt(contribution)
        writeString(rank)
        writeInt(rating)
        writeString(maxRank)
        writeInt(maxRating)
        writeInt(lastOnlineTimeSeconds)
        writeInt(registrationTimeSeconds)
        writeInt(friendOfCount)
        writeString(avatar)
        writeString(titlePhoto)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<User> = object : Parcelable.Creator<User> {
            override fun createFromParcel(source: Parcel): User = User(source)
            override fun newArray(size: Int): Array<User?> = arrayOfNulls(size)
        }
    }
}