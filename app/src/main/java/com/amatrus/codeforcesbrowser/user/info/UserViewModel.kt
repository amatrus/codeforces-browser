package com.amatrus.codeforcesbrowser.user.info

import android.databinding.ObservableInt
import android.util.Log
import android.view.View
import java.util.*
import android.databinding.BindingAdapter
import android.databinding.ObservableBoolean
import android.support.v4.content.ContextCompat
import android.widget.TextView
import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.base.BaseUserViewModel
import com.amatrus.codeforcesbrowser.extensions.convertTimeSecondsToDate


/**
 * Created by engnaruto on 11/18/17.
 */

class UserViewModel : BaseUserViewModel() {
    var dataLayoutVisibility: ObservableInt = ObservableInt(View.GONE)

    var nameRowVisibility: ObservableInt = ObservableInt(View.GONE)
    var emailRowVisibility: ObservableInt = ObservableInt(View.GONE)
    var homeRowVisibility: ObservableInt = ObservableInt(View.GONE)
    var organizationRowVisibility: ObservableInt = ObservableInt(View.GONE)
    var openIdRowVisibility: ObservableInt = ObservableInt(View.GONE)
    var vkIdRowVisibility: ObservableInt = ObservableInt(View.GONE)

    var isOnline: ObservableBoolean = ObservableBoolean(false)

    override fun showResults() {
        super.showResults()
        dataLayoutVisibility.set(View.VISIBLE)
    }

    override fun showErrorMessage(errorMessage: String) {
        dataLayoutVisibility.set(View.GONE)
        super.showErrorMessage(errorMessage)
    }

    //    @SerializedName("email") val email: String = "", //tarekcsed@yahoo.com
    fun email(): String {
        if (user.email != "") {
            emailRowVisibility.set(View.VISIBLE)
        }
        return user.email
    }


    //    @SerializedName("vkId") val vkId: String = "", //UNKNOWN
    fun vkId(): String {
        if (user.vkId != "") {
            vkIdRowVisibility.set(View.VISIBLE)
        }
        return user.vkId
    }

    //    @SerializedName("openId") val openId: String = "", //UNKNOWN
    fun openId(): String {
        if (user.openId != "") {
            openIdRowVisibility.set(View.VISIBLE)
        }
        return user.openId
    }

    //    @SerializedName("lastName") val lastName: String = "", //Khaled
    //    @SerializedName("firstName") val firstName: String = "", //Tarek
    fun userName(): String {
        if (user.firstName == "" && user.lastName == "") {
            return "N/A"
        }

        nameRowVisibility.set(View.VISIBLE)

        if (user.lastName == "") {
            return user.firstName
        } else if (user.firstName == "") {
            return user.lastName
        }
        return "${user.firstName} ${user.lastName}"
    }

    //    @SerializedName("country") val country: String = "", //Egypt
    //    @SerializedName("city") val city: String = "", //Alexandria
    fun home(): String {
        if (user.city == "" && user.country == "") {
            return "N/A"
        }
        homeRowVisibility.set(View.VISIBLE)
        if (user.country == "") {
            return user.city
        } else if (user.city == "") {
            return user.country
        }
        return "${user.city}, ${user.country}"
    }

    //    @SerializedName("organization") val organization: String = "", //University of Alexandria, Faculty of Engineering
    fun organization(): String {
        if (user.organization != "") {
            organizationRowVisibility.set(View.VISIBLE)
        }
        return user.organization
    }

    //    @SerializedName("contribution") val contribution: Int = 0, //0
    fun contribution(): String {
        if (user.contribution == 1) {
            return "${user.contribution} contribution"
        }
        return "${user.contribution} contributions"
    }

    //    @SerializedName("maxRank") val maxRank: String = "", //specialist
    fun maxRank(): String = user.maxRank.capitalize()
            .split(" ").joinToString(" ") { it.capitalize() }

    //    @SerializedName("maxRating") val maxRating: Int = 0, //1411
    fun maxRating(): String = user.maxRating.toString()

    //    @SerializedName("lastOnlineTimeSeconds") val lastOnlineTimeSeconds: Int = 0, //1511112909
    fun lastOnlineTime(): String {
        val timeFormat = "dd/MMM/yyyy HH"
        val formattedLastOnlineTime = user.lastOnlineTimeSeconds
                .convertTimeSecondsToDate(timeFormat)
        val currentTime = Date().time.convertTimeSecondsToDate(timeFormat)

        Log.d("Time", "OnlineTime = $formattedLastOnlineTime")
        Log.d("Time", "CurrentTime = $currentTime")


        if (currentTime == formattedLastOnlineTime) {
            isOnline.set(true)
            return "Online Now"
        } else {
            isOnline.set(false)

            val onlineTimeList = formattedLastOnlineTime.split(" ")
            val currentTimeList = currentTime.split(" ")
            val timeHours: Int
            return if (onlineTimeList[0] == currentTimeList[0]) {
                timeHours = currentTimeList[1].toInt() - onlineTimeList[1].toInt()

                Log.d("Time", "currentTimeHours = ${currentTimeList[1].toInt()}")
                Log.d("Time", "onlineTimeHours = ${onlineTimeList[1].toInt()}")
                Log.d("Time", "timeHoursDiff = $timeHours")

                if (timeHours == 1) {
                    "an hour ago"
                } else {
                    "$timeHours hours ago"
                }
            } else {
                onlineTimeList[0]
            }
        }
    }

    //    @SerializedName("registrationTimeSeconds") val registrationTimeSeconds: Int = 0, //1353279110
    fun registrationTime(): String {
        val dateFormat = "dd/MMM/yyyy"
        return user.registrationTimeSeconds.convertTimeSecondsToDate(dateFormat)
    }

    //    @SerializedName("friendOfCount") val friendOfCount: Int = 0, //34
    fun friendOfCount(): String {
        if (user.friendOfCount == 1) {
            return "${user.friendOfCount} user"
        }
        return "${user.friendOfCount} users"
    }

    object TextViewBindingAdapter {

        @BindingAdapter("color")
        @JvmStatic
        fun setFont(textView: TextView, isOnline: Boolean) {
            val color = if (isOnline) {
                R.color.userColorGreen
            } else {
                R.color.userColorBlack
            }
            textView.setTextColor(ContextCompat.getColor(textView.context, color))
        }
    }

    //    @SerializedName("avatar") val avatar: String = "", //http://userpic.codeforces.com/74645/avatar/64e9480ceb3bba47.jpg
    //    fun userAvatar(): String = user.avatar
}