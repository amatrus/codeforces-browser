package com.amatrus.codeforcesbrowser.user.info


import android.databinding.DataBindingUtil
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.amatrus.codeforcesbrowser.R
import com.amatrus.codeforcesbrowser.databinding.UserInfoFragmentBinding
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [UserInfoFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class UserInfoFragment : Fragment(), Observer {
    lateinit var binding: UserInfoFragmentBinding
    lateinit var userViewModel: UserViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.user_info_fragment, container, false)

        val handle = PreferenceManager.getDefaultSharedPreferences(activity).getString("handle", "").trim()

        initializeBinding(handle)
        setupObserver(userViewModel)

        return binding.root
    }

    private fun initializeBinding(handle: String) {
        userViewModel = UserViewModel()
        userViewModel.getUser(handle)
        binding.viewModel = userViewModel
    }

    private fun setupObserver(observable: Observable) {
        observable.addObserver(this)
    }

    override fun update(p0: Observable?, p1: Any?) {
        if (p0 is UserViewModel) {
            binding.viewModel = userViewModel
        }
    }

    companion object {
        fun newInstance(): UserInfoFragment = UserInfoFragment()
    }

}// Required empty public constructor
